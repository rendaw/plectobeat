package com.zarbosoft.plectobeat.visual;

import com.zarbosoft.plectobeat.Context;
import com.zarbosoft.plectobeat.config.deviceset.DeviceSetDef_0_0_1;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

import static com.zarbosoft.plectobeat.Main.localization;

public class StageDeviceSets extends BaseStage {
	private final Context context;

	public StageDeviceSets(Context context) {
		this.context = context;
		setTitle(localization.getString("manage.device.sets"));

		setScene(new Scene(new Builder.ListBuilder<DeviceSetDef_0_0_1>(context.devices,
				() -> new ListCell<DeviceSetDef_0_0_1>() {
					@Override
					protected void updateItem(DeviceSetDef_0_0_1 item, boolean empty) {
						super.updateItem(item, empty);
						if (item == null)
							textProperty().unbind();
						else
							textProperty().bind(item.name);
					}
				}
		).addNewDelete(DeviceSetDef_0_0_1::createDefault, null).addDuplicate(d -> {
			DeviceSetDef_0_0_1 out = d.cloneFull();
			out.name.set(String.format(localization.getString("s.copy"), out.name.get()));
			return out;
		}).addButton(devices -> {
			Button edit = new Button(localization.getString("edit"));
			edit.setMaxWidth(150);
			edit.setOnAction(e -> {
				DeviceSetDef_0_0_1 selected = devices.getSelectionModel().getSelectedItem();
				if (selected == null)
					return;
				if (context.stageEditDeviceSet != null)
					if (context.stageEditDeviceSet.device == selected) {
						context.stageEditDeviceSet.requestFocus();
						return;
					} else {
						context.stageEditDeviceSet.close();
					}
				context.stageEditDeviceSet = new StageEditDeviceSet(context, selected);
			});
			return edit;
		}).addButton(list -> {
			Pane buttonSeparator = new Pane();
			VBox.setVgrow(buttonSeparator, Priority.ALWAYS);
			return buttonSeparator;
		}).addButton(list -> {
			Button close = new Button(localization.getString("close"));
			close.setMaxWidth(150);
			close.setOnAction(e -> {
				close();
			});
			return close;
		}).select(context.device).build()));
		show();
	}

	@Override
	public void close() {
		super.close();
		context.devicesStage = null;
		context.save();
	}
}
