package com.zarbosoft.plectobeat.visual;

import com.zarbosoft.interface1.TypeInfo;
import com.zarbosoft.luxem.Luxem;
import com.zarbosoft.luxem.write.TypeWriter;
import com.zarbosoft.plectobeat.InputEvent;
import com.zarbosoft.plectobeat.*;
import com.zarbosoft.plectobeat.config.appstate.AppState_0_0_1;
import com.zarbosoft.plectobeat.config.deviceset.DeviceSetDef_0_0_1;
import com.zarbosoft.plectobeat.config.profile.ProfileDef_0_0_1;
import com.zarbosoft.plectobeat.config.trackmeta.TrackMeta_0_0_1;
import com.zarbosoft.rendaw.common.Pair;
import javafx.animation.AnimationTimer;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.converter.NumberStringConverter;
import org.apache.batik.dom.svg.SVGDOMImplementation;
import org.apache.batik.ext.awt.image.codec.PNGEncodeParam;
import org.apache.batik.ext.awt.image.codec.PNGImageEncoder;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.TranscodingHints;
import org.apache.batik.transcoder.image.ImageTranscoder;
import org.apache.batik.util.SVGConstants;

import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.text.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static com.zarbosoft.plectobeat.Main.*;
import static com.zarbosoft.plectobeat.config.profile.ProfileDef_0_0_1.AudioMode.*;
import static com.zarbosoft.plectobeat.visual.BaseStage.appIcon;
import static com.zarbosoft.rendaw.common.Common.*;

public class StageMain {
	public static class TimeParts {
		boolean negative;
		int seconds;
		int minutes;
		int hours;
	}

	public static TimeParts timeParts = new TimeParts();

	public static TimeParts splitTime(long usec) {
		timeParts = new TimeParts();
		timeParts.negative = usec < 0;
		timeParts.seconds = (int) (usec / (1000 * 1000)) * (timeParts.negative ? -1 : 1);
		timeParts.minutes = (int) (timeParts.seconds / (60));
		timeParts.hours = (int) (timeParts.minutes / (60));
		timeParts.seconds = timeParts.seconds % 60;
		timeParts.minutes = timeParts.minutes % 60;
		return timeParts;
	}

	public static String formatTime(long v) {
		TimeParts out = splitTime(v);
		if (out.hours > 0) {
			return String.format("%s%s:%02d:%02d", out.negative ? "-" : "", out.hours, out.minutes, out.seconds);
		} else {
			return String.format("%s%02d:%02d", out.negative ? "-" : "", out.minutes, out.seconds);
		}
	}

	public static Color scoreColor(double x) {
		return Color.hsb(260 + 110 * (Math.cos(Math.PI + x * Math.PI) * 0.5 + 0.5), 0.8 - 0.1 * x * x, 0.6 + x * 0.4);
	}

	public static void setup(Context context, Stage primaryStage) {
		Label timeLabel = hardLabel();
		timeLabel.setMaxHeight(100);
		GridPane.setHgrow(timeLabel, Priority.ALWAYS);
		timeLabel.setAlignment(Pos.BASELINE_CENTER);

		Label barLabel = hardLabel();
		barLabel.setMaxHeight(100);
		GridPane.setHgrow(barLabel, Priority.ALWAYS);
		barLabel.setAlignment(Pos.BASELINE_CENTER);

		Button timeButton = mainStageButton(null, "clock-outline.svg");
		timeButton.setTooltip(new Tooltip(localization.getString("set.time")));
		timeButton.setOnAction(e -> {
			class SetTimeStage extends BaseStage {
				SetTimeStage(Stage modalParent) {
					setTitle(localization.getString("set.time"));

					NumberFormat format = DecimalFormat.getIntegerInstance();
					Supplier<TextFormatter> createFormatter = () -> new TextFormatter<>(c -> {
						if (c.getControlNewText().isEmpty())
							return c;
						ParsePosition parsePosition = new ParsePosition(0);
						Object object = format.parse(c.getControlNewText(), parsePosition);
						if (object == null || parsePosition.getIndex() < c.getControlNewText().length())
							return null;
						else
							return c;
					});

					TimeParts timeParts = splitTime(context.timeSource.getNow());
					HBox hBox = new HBox();
					TextField timeLeft = new TextField();
					timeLeft.setTextFormatter(createFormatter.get());
					timeLeft.setText(Integer.toString(timeParts.hours * 60 + timeParts.minutes));
					TextField timeRight = new TextField();
					timeRight.setTextFormatter(createFormatter.get());
					timeRight.setText(Integer.toString(timeParts.seconds));
					hBox.getChildren().addAll(timeLeft, new Label(" : "), timeRight);

					Button close = new Button(localization.getString("close"));
					close.setOnAction(e -> {
						close();
					});
					Button set = new Button(localization.getString("set"));
					set.setOnAction(e -> {
						context.seek((
								(
										timeLeft.getText().isEmpty() ? 0 : Integer.parseInt(timeLeft.getText())
								) * 60 + (
										timeRight.getText().isEmpty() ? 0 : Integer.parseInt(timeRight.getText())
								)
						) * 1000 * 1000);
						close();
					});
					set.setDefaultButton(true);
					HBox closeBox = new HBox();
					closeBox.setSpacing(8);
					closeBox.setPadding(new Insets(4, 4, 4, 4));
					closeBox.setAlignment(Pos.CENTER_RIGHT);
					closeBox.getChildren().addAll(set, close);

					VBox topLayout = new VBox();
					topLayout.getChildren().addAll(hBox, closeBox);

					setScene(new Scene(topLayout));

					initOwner(modalParent);
					initModality(Modality.APPLICATION_MODAL);
					showAndWait();
				}
			}
			new SetTimeStage(primaryStage);
		});

		context.canvas = new Pane();
		context.canvas.setFocusTraversable(true);
		context.canvas.setMouseTransparent(false);
		{
			final Rectangle outputClip = new Rectangle();
			context.canvas.setClip(outputClip);
			context.canvas.layoutBoundsProperty().addListener((ov, oldValue, newValue) -> {
				outputClip.setWidth(newValue.getWidth());
				outputClip.setHeight(newValue.getHeight());
			});
		}
		context.canvas.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
		VBox.setVgrow(context.canvas, Priority.ALWAYS);
		context.barVisuals.forEach(pair -> context.canvas.getChildren().addAll(pair.second, pair.first));
		context.canvas.getChildren().addAll(context.beatVisuals);
		context.canvas.widthProperty().addListener((
				(observable, oldValue, newValue) -> {
					context.width = newValue.doubleValue();
					for (Pair<Label, VisualBar> bar : context.barVisuals) {
						bar.first.setFont(Font.font("SansSerif", newValue.doubleValue() / 30));
						bar.first.setLayoutX(context.width * 0.05);
						bar.second.resize(context);
					}
					for (VisualBar bar : context.beatVisuals)
						bar.resize(context);
					resizeAndDistributeColumns(context);
				}
		));
		context.canvas.heightProperty().addListener((
				(observable, oldValue, newValue) -> {
					context.height = newValue.doubleValue();
					resizeAndDistributeColumns(context);
				}
		));
		context.canvas.addEventFilter(MouseEvent.MOUSE_ENTERED, e -> {
			context.canvas.requestFocus();
		});
		class ScrollEventState {
			boolean longScroll = false;
		}
		ScrollEventState scrollEventState = new ScrollEventState();
		context.canvas.addEventFilter(ScrollEvent.SCROLL_STARTED, e -> {
			scrollEventState.longScroll = true;
		});
		context.canvas.addEventFilter(ScrollEvent.SCROLL_FINISHED, e1 -> {
			scrollEventState.longScroll = false;
		});
		context.canvas.addEventFilter(ScrollEvent.SCROLL, e1 -> {
			if (!scrollEventState.longScroll) {
				context.transportSkipForward(0.1 * (
						e1.getTextDeltaYUnits() == ScrollEvent.VerticalTextScrollUnits.NONE ?
								e1.getDeltaY() / 40 :
								e1.getTextDeltaY()
				));
			} else {
				context.transportSkipForward(0.1 * (e1.getTotalDeltaY() / 40));
			}
		});
		AnimationTimer animate = new AnimationTimer() {
			@Override
			public void handle(long now1) {
				// usec
				long now = context.timeSource.getNow(now1);

				if (context.seek) {
					long seekWindowStart = now - (long) (context.profile.windowTimeAfter * 1000 * context.floatSpeed);
					for (Column column : context.columns) {
						boolean beforeWindow = true;
						for (int at = 0; at < column.events.size(); ++at) {
							ColumnEvent event = column.events.get(at);

							// Move forward to first element that passes window edge
							if (beforeWindow) {
								column.at = at;
								if (event.end >= seekWindowStart)
									beforeWindow = false;
							}

							// Reset note states
							if (event.end < context.seekNotes) {
								event.startHit = HitState.HIT;
								event.endHit = HitState.HIT;
							} else {
								event.startHit = HitState.PENDING;
								event.endHit = HitState.PENDING;
							}
						}
					}
					context.seek = false;
					context.lastNow = now;
				} else {
					long realNow = stamp();
					long diff = realNow - context.lastRealNow;
					if (diff > 0 && diff < 1000)
						context.recordTime(diff * 1000);
					context.lastRealNow = realNow;
					context.lastNow = now;
				}

				context.startManager.update(context);
				final long windowStart = now - (long) (context.profile.windowTimeAfter * 1000 * context.floatSpeed);
				final long windowEnd = now + (long) (context.profile.windowTimeBefore * 1000 * context.floatSpeed);

				// Process bar lines
				{
					final long barTimePadding = context.pixelToUsec(100) * 2;
					int beatVisualAt = 0;
					int barVisualAt = 0;
					context.currentTimeSignature =
							context.beatWalker.adjust(windowStart - barTimePadding, windowEnd + barTimePadding);
					for (BeatWalker.Event event : context.beatWalker) {
						if (event.timeSignature.start <= now)
							context.currentTimeSignature = event.timeSignature;
						if (barVisualAt < context.barVisuals.size() && beatVisualAt < context.beatVisuals.size()) {
							VisualBar visual;
							double y = (event.start - windowStart) / (float) (windowEnd - windowStart) *
									context.canvas.heightProperty().get();
							y = context.canvas.heightProperty().get() - y;
							if (event.index % event.timeSignature.barMultiple == 0) {
								Pair<Label, VisualBar> pair = context.barVisuals.get(barVisualAt++);
								visual = pair.second;
								pair.first.setText(Long.toString(event.timeSignature.barOffset +
										event.index / event.timeSignature.barMultiple));
								pair.first.setLayoutY(y - pair.first.getHeight());
								pair.first.setVisible(true);
							} else {
								visual = context.beatVisuals.get(beatVisualAt++);
							}
							visual.setLayoutY(y);
							visual.setVisible(true);
						}
					}

					// Hide no longer used visuals
					for (; barVisualAt < context.barVisuals.size(); ++barVisualAt) {
						Pair<Label, VisualBar> pair = context.barVisuals.get(barVisualAt);
						if (!pair.first.isVisible())
							break;
						pair.first.setVisible(false);
						pair.second.setVisible(false);
					}
					for (; beatVisualAt < context.beatVisuals.size(); ++beatVisualAt) {
						VisualBar visual = context.beatVisuals.get(beatVisualAt);
						if (!visual.isVisible())
							break;
						visual.setVisible(false);
					}
				}

				context.mistakeManager.processNow(context, now, context.currentTimeSignature);

				// Process notes
				final long eventTimePadding = context.pixelToUsec(context.eventRadius() * 2) * 2;
				for (int columnIndex = 0; columnIndex < context.columns.size(); ++columnIndex) {
					Column column = context.columns.get(columnIndex);
					int visualAt = 0;
					int columnAt = column.at;
					int detect = (context.profile.detectMs + column.def.detectMs) * 1000;

					boolean triggeredStart = false;
					boolean triggeredEnd = false;
					for (InputEvent inputEvent : context.events)
						for (DeviceSetDef_0_0_1.ColumnTrigger trigger : column.def.triggers)
							if (trigger.match(inputEvent)) {
								if (inputEvent.on)
									triggeredStart = true;
								else
									triggeredEnd = true;
								column.visual.setHit();
								break;
							}

					while (true) {
						if (columnAt >= column.events.size())
							break;
						if (visualAt >= column.eventVisuals.size())
							break;
						ColumnEvent event = column.events.get(columnAt);

						if (event.end < windowStart - eventTimePadding) {
							column.at += 1;
							columnAt = column.at;
						} else {
							if (event.start >= windowEnd + eventTimePadding)
								break;

							// Catch hits
							if (event.startHit == HitState.PENDING && event.start <= now + detect && triggeredStart) {
								event.startHit = HitState.HIT;
								context.recordHit();
							}
							if (event.endHit == HitState.PENDING && event.end <= now + detect && triggeredEnd) {
								event.endHit = HitState.HIT;
								context.recordHit();
							}

							// Draw
							VisualEvent visual = column.eventVisuals.get(visualAt++);
							visual.set(context, column.def, now, event);

							// Check for mistakes
							context.mistakeManager.processEvent(context, now, column, event);

							columnAt += 1;
						}
					}
					for (; visualAt < column.eventVisuals.size(); ++visualAt) {
						if (!column.eventVisuals.get(visualAt).isVisible())
							break;
						column.eventVisuals.get(visualAt).setVisible(false);
					}
					column.visual.updateFill();
				}
				context.events.drainTo(context.eventsPool);

				timeLabel
						.textProperty()
						.setValue(String.format("%s / %s", formatTime(now), formatTime(context.trackMeta.lengthUsec)));
				{
					TimeSignatureEvent currentTimeSignature = context.currentTimeSignature;
					TimeSignatureEvent lastTimeSignature = last(context.timeSignatureEvents);
					long timeBeats = (now - currentTimeSignature.start) / currentTimeSignature.beatLength;
					barLabel.textProperty().setValue(String.format("%s:%s / %s:0",
							currentTimeSignature.barOffset + timeBeats / currentTimeSignature.barMultiple,
							timeBeats % currentTimeSignature.barMultiple,
							(context.trackMeta.lengthUsec - lastTimeSignature.start) /
									lastTimeSignature.beatLength /
									lastTimeSignature.barMultiple
					));
				}
			}
		};
		animate.start();

		HBox transport = new HBox();
		transport.paddingProperty().setValue(new Insets(4, 4, 4, 4));
		transport.setSpacing(4);
		ToggleGroup trackMode = new ToggleGroup();
		ToggleButton modeSilent = mainStageToggleButton(null, "audio-mode-silence.svg");
		ToggleButton modeMetronome = mainStageToggleButton(null, "audio-mode-metronome.svg");
		ToggleButton modeMusic = mainStageToggleButton(null, "audio-mode-song.svg");
		{
			Button previous = mainStageButton(null, "skip-previous.svg");
			previous.setOnAction(event -> {
				context.previousTrack();
			});
			Button rewind = mainStageButton(null, "reload.svg");
			rewind.setOnAction(event -> {
				context.reset();
				context.seek(0);
			});
			Button jumpBack = mainStageButton(null, "rewind.svg");
			jumpBack.setOnAction(event -> {
				context.transportSkipBack();
			});
			Button stop = mainStageButton(null, "pause.svg");
			stop.setOnAction(event -> {
				context.transportStop();
			});
			Button jumpForward = mainStageButton(null, "fast-forward.svg");
			jumpForward.setOnAction(event -> {
				context.transportSkipForward();
			});
			Button start = mainStageButton(null, "play.svg");
			start.setOnAction(event -> {
				context.transportPlay();
			});
			Button next = mainStageButton(null, "skip-next.svg");
			next.setOnAction(event -> {
				context.nextTrack();
			});
			ComboBox<String> speedCombo = speedCombo(context.speed);
			speedCombo.setOnKeyPressed(e -> {
				if (e.getCode() == KeyCode.ESCAPE) {
					primaryStage.requestFocus();
					e.consume();
					return;
				}
			});
			speedCombo.setFocusTraversable(false);
			context.speed.addListener((observable, oldValue, newValue) -> {
				context.timeSource.setSpeed(newValue.intValue());
			});
			transport.getChildren().addAll(previous, rewind, jumpBack, stop, start, jumpForward, next);
			addLabeled(transport, localization.getString("speed"), speedCombo, null);

			modeSilent.setTooltip(new Tooltip(localization.getString("silent")));
			modeSilent.selectedProperty().addListener((observable, oldValue, newValue) -> {
				if (!newValue)
					return;
				if (context.profile.audioMode == SILENCE)
					return;
				context.profile.audioMode = SILENCE;
				context.createTimeSource();
			});
			modeSilent.setToggleGroup(trackMode);
			modeMetronome.setTooltip(new Tooltip(localization.getString("metronome")));
			modeMetronome.selectedProperty().addListener((observable, oldValue, newValue) -> {
				if (!newValue)
					return;
				if (context.profile.audioMode == METRONOME)
					return;
				context.profile.audioMode = METRONOME;
				context.createTimeSource();
			});
			modeMetronome.setToggleGroup(trackMode);
			modeMusic.setTooltip(new Tooltip(localization.getString("music")));
			modeMusic.selectedProperty().addListener((observable, oldValue, newValue) -> {
				if (!newValue)
					return;
				if (context.profile.audioMode == SONG)
					return;
				context.profile.audioMode = SONG;
				context.createTimeSource();
			});
			modeMusic.setToggleGroup(trackMode);
			trackMode.selectToggle(context.profile.audioMode == SONG ?
					modeMusic :
					context.profile.audioMode == METRONOME ? modeMetronome : modeSilent);
			transport.getChildren().addAll(modeSilent, modeMetronome, modeMusic);

			Pane transportSpace = new Pane();
			HBox.setHgrow(transportSpace, Priority.ALWAYS);
			transport.getChildren().add(transportSpace);
		}
		{
			ToggleButton audible = mainStageToggleButton(null, "volume-high.svg");
			audible.setTooltip(new Tooltip(localization.getString("audible")));
			audible.selectedProperty().bindBidirectional(context.appState.audible);
			Slider volume = new Slider();
			volume.setFocusTraversable(false);
			volume.setMin(0);
			volume.setMax(1000);
			volume.setMaxHeight(100);
			volume.valueProperty().bindBidirectional(context.appState.volume);
			transport.getChildren().addAll(audible, volume);
		}

		GridPane left = new Form();
		left.setBorder(new Border(new BorderStroke(Color.GRAY,
				BorderStrokeStyle.SOLID,
				CornerRadii.EMPTY,
				new BorderWidths(0, 1, 0, 0)
		)));
		left.setMinWidth(200);
		int leftRow = 0;

		Label comboIndicator = hardLabel();
		comboIndicator.setMaxHeight(100);
		comboIndicator.setAlignment(Pos.BASELINE_CENTER);
		GridPane.setHgrow(comboIndicator, Priority.ALWAYS);
		comboIndicator.textProperty().bind(context.combo.asString().concat("x"));

		ProgressBar score = new ProgressBar();
		score.setMaxWidth(1000);
		HBox.setHgrow(score, Priority.ALWAYS);
		score.progressProperty().addListener(new ChangeListener<Number>() {
			CornerRadii radii = new CornerRadii(2);
			Insets insets = new Insets(3);
			Pane fill;

			@Override
			public void changed(
					ObservableValue<? extends Number> observable, Number oldValue, Number newValue
			) {
				if (fill == null)
					fill = (Pane) score.lookup(".bar");
				if (fill == null)
					return;
				double x = newValue.doubleValue();
				fill.setBackground(new Background(new BackgroundFill(scoreColor(x), radii, insets)));
			}
		});
		score.progressProperty().bind(context.score);
		Label scoreText = new Label();
		scoreText.setMaxWidth(1000);
		scoreText.textProperty().bind(context.score.multiply(100.0).asString("%.0f"));
		scoreText.setAlignment(Pos.BASELINE_CENTER);

		ListView<AppState_0_0_1.PlaylistEntry> playlist = new ListView(context.appState.playlist);
		playlist.focusTraversableProperty().setValue(false);
		playlist.setCellFactory(v -> {
			return new ListCell<AppState_0_0_1.PlaylistEntry>() {
				private final HBox display;
				private final ImageView graphic;
				private final Label name;
				private final Label score;

				{
					display = new HBox();
					graphic = icon("flower.svg");
					name = new Label();
					Pane space = new Pane();
					HBox.setHgrow(space, Priority.ALWAYS);
					score = new Label();
					display.getChildren().addAll(graphic, name, space, score);
					setGraphic(display);
				}

				@Override
				protected void updateItem(AppState_0_0_1.PlaylistEntry item, boolean empty) {
					super.updateItem(item, empty);
					if (item == null)
						getGraphic().setVisible(false);
					else {
						getGraphic().setVisible(true);
						graphic.visibleProperty().bind(item.loaded);
						name.setText(item.name);
						score.textProperty().bind(Bindings.format("(%.0f)", item.score.multiply(100)));
						score
								.textFillProperty()
								.bind(Bindings.createObjectBinding(() -> scoreColor(item.score.get()), item.score));
						score.visibleProperty().bind(item.score.isNotEqualTo(-1));
					}
				}
			};
		});
		playlist.setOnMouseClicked(e -> {
			if (!(e.getButton() == MouseButton.PRIMARY && e.getClickCount() == 2))
				return;
			AppState_0_0_1.PlaylistEntry entry = playlist.getSelectionModel().getSelectedItem();
			context.load(entry);
		});
		{
			ScrollPane playlistScroll = new ScrollPane(playlist);
			playlistScroll.fitToHeightProperty().setValue(true);
			playlistScroll.fitToWidthProperty().setValue(true);
			playlistScroll.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
			playlistScroll.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
			VBox.setVgrow(playlistScroll, Priority.ALWAYS);
			Button add = mainStageButton(null, "plus-circle-outline.svg");
			add.setTooltip(new Tooltip("Add"));
			add.setOnAction(e -> {
				FileChooser fileChooser = new FileChooser();
				fileChooser.setTitle(localization.getString("open.song"));
				fileChooser.setInitialDirectory(trackPath.toFile());
				fileChooser
						.getExtensionFilters()
						.addAll(new FileChooser.ExtensionFilter(localization.getString("all.songs"),
										"*." + songExtension,
										"*.mid",
										"*.midi"
								),
								new FileChooser.ExtensionFilter(localization.getString("plectotrack"),
										"*." + songExtension
								),
								new FileChooser.ExtensionFilter(localization.getString("midi"), "*.mid", "*.midi")
						);
				File f = fileChooser.showOpenDialog(primaryStage);
				if (f == null)
					return;
				TrackMeta_0_0_1 meta = loadMeta(f.toPath());
				AppState_0_0_1.PlaylistEntry entry = new AppState_0_0_1.PlaylistEntry();
				entry.name = meta.name;
				entry.path = f.toPath().toString();
				playlist.getItems().add(entry);
				playlist.getSelectionModel().select(entry);
				context.load(entry);
			});
			Button remove = mainStageButton(null, "minus-circle-outline.svg");
			remove.setTooltip(new Tooltip(localization.getString("remove")));
			remove.setOnAction(e -> {
				playlist.getItems().removeAll(playlist.getSelectionModel().getSelectedItems());
				context.loadMidi();
			});
			Button moveUp = mainStageButton(null, "arrow-up-bold-outline.svg");
			moveUp.setTooltip(new Tooltip(localization.getString("move.up")));
			moveUp.setOnAction(e -> {
				Optional<Integer> startIndex =
						playlist.getSelectionModel().getSelectedIndices().stream().sorted().findFirst();
				if (!startIndex.isPresent())
					return;
				if (startIndex.get() == 0)
					return;
				ArrayList<AppState_0_0_1.PlaylistEntry> removed =
						new ArrayList<>(playlist.getSelectionModel().getSelectedItems());
				playlist.getItems().removeAll(removed);
				int dest = startIndex.get() - 1;
				playlist.getItems().addAll(dest, removed);
				playlist.getSelectionModel().selectRange(dest, dest + removed.size());
			});
			Button moveDown = mainStageButton(null, "arrow-down-bold-outline.svg");
			moveDown.setTooltip(new Tooltip("Move down"));
			moveDown.setOnAction(e -> {
				Optional<Integer> startIndex =
						playlist.getSelectionModel().getSelectedIndices().stream().sorted().findFirst();
				if (!startIndex.isPresent())
					return;
				if (startIndex.get() >= playlist.getItems().size() - 1)
					return;
				ArrayList<AppState_0_0_1.PlaylistEntry> removed =
						new ArrayList<>(playlist.getSelectionModel().getSelectedItems());
				playlist.getItems().removeAll(removed);
				int dest = startIndex.get() + 1;
				playlist.getItems().addAll(dest, removed);
				playlist.getSelectionModel().selectRange(dest, dest + removed.size());
			});
			Button save = mainStageButton(null, "content-save.svg");
			save.setTooltip(new Tooltip(localization.getString("save.playlist")));
			save.setOnAction(e -> {
				FileChooser fileChooser = new FileChooser();
				fileChooser.setTitle(localization.getString("save.playlist"));
				fileChooser
						.getExtensionFilters()
						.addAll(new FileChooser.ExtensionFilter(localization.getString("playlist"), "*.plelist"));
				File f = fileChooser.showSaveDialog(primaryStage);
				if (f == null)
					return;
				uncheck(() -> {
					try (OutputStream out = Files.newOutputStream(f.toPath())) {
						TypeWriter writer = new TypeWriter(out, saveTypeMap).pretty((byte) ' ', 4);
						for (AppState_0_0_1.PlaylistEntry entry : playlist.getItems())
							writer.write(new TypeInfo(List.class, new TypeInfo(AppState_0_0_1.PlaylistEntry.class)),
									entry
							);
					}
				});
			});
			Button load = mainStageButton(null, "folder-outline.svg");
			load.setTooltip(new Tooltip(localization.getString("load.playlist")));
			load.setOnAction(e -> {
				FileChooser fileChooser = new FileChooser();
				fileChooser.setTitle(localization.getString("load.playlist"));
				fileChooser
						.getExtensionFilters()
						.addAll(new FileChooser.ExtensionFilter(localization.getString("playlist"), "*.plelist"));
				File f = fileChooser.showOpenDialog(primaryStage);
				if (f == null)
					return;
				playlist.getItems().clear();
				uncheck(() -> {
					try (InputStream source = Files.newInputStream(f.toPath())) {
						playlist
								.getItems()
								.addAll(Luxem.<AppState_0_0_1.PlaylistEntry>parse(reflections,
										new TypeInfo(AppState_0_0_1.PlaylistEntry.class)
								)
										.map(saveTypeMap)
										.from(source)
										.collect(Collectors.toList()));
					}
				});
			});
			Pane space = new Pane();
			HBox.setHgrow(space, Priority.ALWAYS);
			HBox playlistControls = new HBox();
			playlistControls.setSpacing(2);
			playlistControls.getChildren().addAll(add, remove, moveUp, moveDown, space, save, load);
			VBox playlistAll = new VBox();
			playlistAll.setPadding(new Insets(4, 4, 4, 4));
			playlistAll.setSpacing(4);
			playlistAll.setBorder(new Border(new BorderStroke(Color.GRAY,
					BorderStrokeStyle.SOLID,
					new CornerRadii(4),
					new BorderWidths(1)
			)));
			Label playlistLabel = new Label(localization.getString("playlist"));
			playlistLabel.setFont(Font.font("SansSerif", FontWeight.MEDIUM, 18));
			playlistLabel.setMaxWidth(1000);
			playlistLabel.setAlignment(Pos.BASELINE_CENTER);
			playlistAll.getChildren().addAll(playlistLabel, playlistScroll, playlistControls);
			left.add(playlistAll, 0, leftRow++, 3, 1);
		}

		{
			ComboBox<ProfileDef_0_0_1> combo = new ComboBox<>(context.profiles);
			combo.setFocusTraversable(false);
			combo.getSelectionModel().select(context.profile);
			combo.setCellFactory(view -> new ListCell<ProfileDef_0_0_1>() {
				@Override
				protected void updateItem(ProfileDef_0_0_1 item, boolean empty) {
					super.updateItem(item, empty);
					if (item == null)
						textProperty().unbind();
					else {
						textProperty().bind(item.name);
					}
				}
			});
			combo.setButtonCell(combo.getCellFactory().call(null));
			combo.setMaxWidth(150);
			combo.valueProperty().addListener((
					observable, oldValue, newValue
			) -> {
				context.setProfile(newValue);
				trackMode.selectToggle(newValue.audioMode == SONG ?
						modeMusic :
						newValue.audioMode == METRONOME ? modeMetronome : modeSilent);
			});
			Button config = mainStageButton(null, "settings.svg");
			config.setOnAction(e -> {
				if (context.stageProfiles == null)
					context.stageProfiles = new StageProfiles(context);
				context.stageProfiles.requestFocus();
			});
			left.addRow(leftRow++, hardLabel(localization.getString("profile")), combo, config);
		}

		{
			Button config = mainStageButton(null, "settings.svg");
			config.setOnAction(e -> {
				if (context.devicesStage == null)
					context.devicesStage = new StageDeviceSets(context);
				context.devicesStage.requestFocus();
			});
			ComboBox dropdown = deviceDropdown(context, context::setDevice);
			dropdown.setFocusTraversable(false);
			left.addRow(leftRow++, hardLabel(localization.getString("device")), dropdown, config);
		}

		Label practiceTimeIndicator = hardLabel();
		GridPane.setHgrow(practiceTimeIndicator, Priority.ALWAYS);
		practiceTimeIndicator.setAlignment(Pos.BASELINE_CENTER);
		practiceTimeIndicator
				.textProperty()
				.bind(Bindings.createStringBinding(() -> formatTime(context.timePracticed.get()),
						context.timePracticed
				));
		left.addRow(leftRow++, hardLabel(localization.getString("practice")), practiceTimeIndicator);
		left.addRow(leftRow++, hardLabel(localization.getString("time")), timeLabel, timeButton);
		left.addRow(leftRow++, hardLabel(localization.getString("bar")), barLabel);
		left.addRow(leftRow++, hardLabel(localization.getString("combo")), comboIndicator);
		left.addRow(leftRow++, hardLabel(localization.getString("score")), score, scoreText);

		VBox canvasLayout = new VBox();
		HBox.setHgrow(canvasLayout, Priority.ALWAYS);
		canvasLayout.getChildren().addAll(transport, context.canvas);

		HBox topLayout = new HBox();
		topLayout.getChildren().addAll(left, canvasLayout);

		context.appState.lastTrack.addListener((
				(observable, oldValue, newValue) -> {
					if (newValue == null) {
						primaryStage.setTitle(appName);
					} else {
						primaryStage.setTitle(String.format("%s - %s", appName, newValue.name));
					}
				}
		));
		context.appState.audible.addListener((observable, oldValue, newValue) -> {
			context.applyVolume();
		});
		context.appState.volume.addListener((observable, oldValue, newValue) -> {
			context.applyVolume();
		});

		Scene scene = new Scene(topLayout);
		scene.addEventFilter(KeyEvent.KEY_PRESSED, e -> {
			switch (e.getCode()) {
				case ESCAPE:
					context.transportStop();
					break;
				case LEFT:
					context.transportSkipBack();
					break;
				case RIGHT:
					context.transportSkipForward();
					break;
				case M:
					context.appState.audible.set(!context.appState.audible.get());
					break;
				case SPACE:
					if (context.timeSource.playing())
						context.transportStop();
					else
						context.transportPlay();
					break;
				case UP:
					context.previousTrack();
					break;
				case DOWN:
					context.nextTrack();
					break;
				default:
					return;
			}
			e.consume();
		});
		primaryStage.setScene(scene);

		primaryStage.setOnCloseRequest(e -> {
			if (context.stageProfiles != null)
				context.stageProfiles.requestFocus();
			else if (context.stageEditProfile != null)
				context.stageEditProfile.requestFocus();
			else if (context.devicesStage != null)
				context.devicesStage.requestFocus();
			else if (context.stageEditDeviceSet != null)
				context.stageEditDeviceSet.requestFocus();
			else
				return;
			e.consume();
		});

		if (context.appState.lastTrack.get() != null) {
			context.appState.playlist
					.stream()
					.filter(e -> Objects.equals(e.path, context.appState.lastTrack.get().path))
					.findFirst()
					.ifPresent(entry -> {
						try {
							context.load(entry);
						} catch (Exception e) {
							logger.warn(String.format(localization.getString("could.not.load.song.s.s.from.last.session"),
									entry.path,
									entry.name
							), e);
							primaryStage.setTitle(appName);
						}
					});
		} else {
			primaryStage.setTitle(appName);
		}
		primaryStage.getIcons().add(appIcon);
		primaryStage.show();
	}

	public static void resizeAndDistributeColumns(Context context) {
		for (Column column : context.columns)
			column.resize(context);
		double columnWidth = context.columnWidth();
		double columnWidthSum = columnWidth * context.columns.size();
		double remainingWidth = context.canvas.widthProperty().get() - columnWidthSum;
		for (int i = 0; i < context.columns.size(); ++i) {
			Column column = context.columns.get(i);
			column.visual.setLayoutX(columnWidth * i + (remainingWidth / (context.columns.size() + 1)) * (i + 1));
		}
	}

	public static void addLabeled(Pane pane, String prefix, Node element, String suffix) {
		HBox labelBox = new HBox();
		if (pane instanceof HBox)
			labelBox.paddingProperty().setValue(new Insets(0, 4, 0, 4));
		Label prefixLabel = new Label(prefix);
		prefixLabel.setAlignment(Pos.CENTER_RIGHT);
		prefixLabel.setMaxHeight(100);
		labelBox.getChildren().addAll(prefixLabel, element);
		if (suffix != null) {
			Label suffixLabel = new Label(suffix);
			suffixLabel.setAlignment(Pos.CENTER_RIGHT);
			suffixLabel.setMaxHeight(100);
			labelBox.getChildren().add(suffixLabel);
		}
		pane.getChildren().add(labelBox);
	}

	public static CheckBox checkBox(boolean initialValue, Consumer<Boolean> cb) {
		CheckBox check = new CheckBox();
		check.selectedProperty().setValue(initialValue);
		check.selectedProperty().addListener((observable, oldValue, newValue) -> cb.accept(newValue));
		return check;
	}

	public static CheckBox checkBox(SimpleBooleanProperty value) {
		CheckBox check = new CheckBox();
		check.selectedProperty().bindBidirectional(value);
		return check;
	}

	public static Node colorEntry(DeviceSetDef_0_0_1.ColorDef initial, Consumer<DeviceSetDef_0_0_1.ColorDef> cb) {
		ColorPicker picker = new ColorPicker(initial.get());
		picker.setOnAction(e -> {
			DeviceSetDef_0_0_1.RGBAColorDef out = new DeviceSetDef_0_0_1.RGBAColorDef();
			out.red = (int) (255 * picker.getValue().getRed());
			out.green = (int) (255 * picker.getValue().getGreen());
			out.blue = (int) (255 * picker.getValue().getBlue());
			cb.accept(out);
		});
		return picker;
	}

	public static ComboBox<DeviceSetDef_0_0_1> deviceDropdown(Context context, Consumer<DeviceSetDef_0_0_1> cb) {
		ComboBox<DeviceSetDef_0_0_1> combo = new ComboBox<>(context.devices);
		combo.setValue(context.device);
		combo.setCellFactory(view -> new ListCell<DeviceSetDef_0_0_1>() {
			@Override
			protected void updateItem(DeviceSetDef_0_0_1 item, boolean empty) {
				super.updateItem(item, empty);
				if (item != null)
					textProperty().bind(item.name);
			}
		});
		combo.setButtonCell(combo.getCellFactory().call(null));
		combo.setMaxWidth(150);
		combo.setMaxHeight(150);
		combo.valueProperty().addListener((
				observable, oldValue, newValue
		) -> {
			cb.accept(newValue);
		});
		return combo;
	}

	public static TextField textEntry(SimpleStringProperty value) {
		TextField field = new TextField();
		field.textProperty().bindBidirectional(value);
		return field;
	}

	public static TextField textEntry(String initial, Consumer<String> cb) {
		TextField field = new TextField(initial);
		field.textProperty().addListener(((observable, oldValue, newValue) -> cb.accept(newValue)));
		return field;
	}

	public static TextField numberEntry(long initial, Consumer<Long> cb) {
		NumberFormat format = DecimalFormat.getIntegerInstance();
		TextField field = new TextField();
		field.setText(Long.toString(initial));
		field.setTextFormatter(new TextFormatter<>(c -> {
			if (c.getControlNewText().isEmpty())
				return c;
			ParsePosition parsePosition = new ParsePosition(0);
			Object object = format.parse(c.getControlNewText(), parsePosition);
			if (object == null || parsePosition.getIndex() < c.getControlNewText().length())
				return null;
			else
				return c;
		}));
		field.textProperty().addListener((
				(observable, oldValue, newValue) -> {
					try {
						cb.accept(Long.parseLong(newValue));
					} catch (NumberFormatException e) {
						// nop
					}
				}
		));
		return field;
	}

	public static TextField numberEntry(SimpleIntegerProperty source) {
		NumberFormat format = DecimalFormat.getIntegerInstance();
		TextField field = new TextField();
		field.textProperty().bindBidirectional(source, new Format() {
			@Override
			public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
				toAppendTo.append(Integer.toString((Integer) obj));
				return toAppendTo;
			}

			@Override
			public Object parseObject(String source, ParsePosition pos) {
				if (source.isEmpty())
					return null;
				pos.setIndex(source.length());
				return Integer.parseInt(source);
			}
		});
		field.setTextFormatter(new TextFormatter<>(c -> {
			if (c.getControlNewText().isEmpty())
				return c;
			ParsePosition parsePosition = new ParsePosition(0);
			Object object = format.parse(c.getControlNewText(), parsePosition);
			if (object == null || parsePosition.getIndex() < c.getControlNewText().length())
				return null;
			else
				return c;
		}));
		return field;
	}

	public static Node nonlinearSlider(int min, int max, long initial, Consumer<Integer> cb) {
		HBox out = new HBox();
		Label indicator = new Label();
		Slider slider = new Slider();
		slider.setMin(0);
		slider.setMax(1);
		double range = max - min;
		slider.setValue(Math.pow((initial - min) / range, 0.5));
		indicator.setText(Long.toString(initial));
		slider.valueProperty().addListener((
				observable, oldValue, newValue
		) -> {
			int value = (int) (Math.pow(newValue.doubleValue(), 2) * range + min);
			indicator.setText(Integer.toString(value));
			cb.accept(value);
		});
		out.getChildren().addAll(slider, indicator);
		return out;
	}

	public static ComboBox<String> speedCombo(Consumer<Integer> cb) {
		ComboBox<String> speedCombo = new ComboBox<>();
		speedCombo.setCellFactory(view -> new ListCell<String>() {
			@Override
			protected void updateItem(String item, boolean empty) {
				super.updateItem(item, empty);
				if (item != null)
					setText(String.format("%s%%", item));
			}
		});
		speedCombo.setButtonCell(speedCombo.getCellFactory().call(null));
		speedCombo.getItems().addAll("100", "90", "80", "70", "60", "50", "40", "30", "20", "10");
		speedCombo.setMinWidth(100);
		speedCombo.setPrefWidth(100);
		speedCombo.setEditable(true);
		speedCombo.setValue("100");
		speedCombo.valueProperty().addListener((
				observable, oldValue, newValue
		) -> {
			cb.accept(Integer.parseInt(newValue));
		});
		return speedCombo;
	}

	public static Label hardLabel() {
		Label out = new Label();
		out.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);
		return out;
	}

	public static Label hardLabel(String text) {
		Label out = hardLabel();
		out.setText(text);
		return out;
	}

	public static ComboBox<String> speedCombo(SimpleIntegerProperty value) {
		ComboBox<String> speedCombo = new ComboBox<>();
		speedCombo.setCellFactory(view -> new ListCell<String>() {
			@Override
			protected void updateItem(String item, boolean empty) {
				super.updateItem(item, empty);
				if (item != null)
					setText(String.format("%s%%", item));
			}
		});
		speedCombo.setButtonCell(speedCombo.getCellFactory().call(null));
		speedCombo
				.getItems()
				.addAll("200", "150", "120", "110", "100", "90", "80", "70", "60", "50", "40", "30", "20", "10");
		speedCombo.setMinWidth(100);
		speedCombo.setPrefWidth(100);
		speedCombo.setEditable(true);
		Bindings.bindBidirectional(speedCombo.valueProperty(), value, new NumberStringConverter());
		speedCombo.setValue("100");
		return speedCombo;
	}

	public static Image iconSize(String resource, int width, int height) {
		return uncheck(() -> {
			TranscodingHints hints = new TranscodingHints();
			hints.put(ImageTranscoder.KEY_WIDTH, (float) width); //your image width
			hints.put(ImageTranscoder.KEY_HEIGHT, (float) height); //your image height
			hints.put(ImageTranscoder.KEY_DOM_IMPLEMENTATION, SVGDOMImplementation.getDOMImplementation());
			hints.put(ImageTranscoder.KEY_DOCUMENT_ELEMENT_NAMESPACE_URI, SVGConstants.SVG_NAMESPACE_URI);
			hints.put(ImageTranscoder.KEY_DOCUMENT_ELEMENT, SVGConstants.SVG_SVG_TAG);
			hints.put(ImageTranscoder.KEY_XML_PARSER_VALIDATING, false);
			final BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
			ImageTranscoder transcoder = new ImageTranscoder() {
				@Override
				public BufferedImage createImage(int i, int i1) {
					return image;
				}

				@Override
				public void writeImage(
						BufferedImage bufferedImage, TranscoderOutput transcoderOutput
				) throws TranscoderException {

				}
			};
			transcoder.setTranscodingHints(hints);
			uncheck(() -> transcoder.transcode(new TranscoderInput(Main.class.getResourceAsStream("icons/" + resource)),
					null
			));
			ByteArrayOutputStream pngOut = new ByteArrayOutputStream();
			new PNGImageEncoder(pngOut, PNGEncodeParam.getDefaultEncodeParam(image)).encode(image);
			return new Image(new ByteArrayInputStream(pngOut.toByteArray()));
		});
	}

	public static ImageView icon(String resource) {
		return new ImageView(iconCache.computeIfAbsent(resource, r -> iconSize(resource, 16, 16)));
	}

	public static Button iconButton(String resource) {
		return new Button(null, icon(resource));
	}

	public static Button mainStageButton(String text, String icon) {
		Button out = new Button(text, icon == null ? null : icon(icon));
		out.setFocusTraversable(false);
		return out;
	}

	public static ToggleButton mainStageToggleButton(String text, String icon) {
		ToggleButton out = new ToggleButton(text, icon == null ? null : icon(icon));
		out.setFocusTraversable(false);
		return out;
	}
}
