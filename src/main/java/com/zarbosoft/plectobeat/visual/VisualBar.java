package com.zarbosoft.plectobeat.visual;

import com.zarbosoft.plectobeat.Context;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class VisualBar extends Canvas {

	private final boolean important;

	public VisualBar(boolean important) {
		this.important = important;
		setHeight(3);
	}

	public void resize(Context context) {
		setWidth(context.width);
		GraphicsContext gc = getGraphicsContext2D();
		gc.clearRect(0, 0, getWidth() + 1, getHeight() + 1);
		gc.beginPath();
		gc.setLineWidth(1);
		gc.setStroke(Color.color(0, 0, 0, important ? 0.9 : 0.3));
		int half = (int) (context.width * (important ? 0.05 : 0.08));
		gc.strokeLine(half, 1.5, context.width - 2 * half, 1.5);
		gc.closePath();
	}
}
