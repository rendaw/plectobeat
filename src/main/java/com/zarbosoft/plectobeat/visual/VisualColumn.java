package com.zarbosoft.plectobeat.visual;

import com.zarbosoft.plectobeat.Context;
import com.zarbosoft.plectobeat.config.deviceset.DeviceSetDef_0_0_1;
import com.zarbosoft.rendaw.common.Pair;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;

import java.time.Instant;
import java.util.stream.Stream;

import static com.zarbosoft.rendaw.common.Common.enumerate;
import static com.zarbosoft.rendaw.common.Common.iterable;

public class VisualColumn extends Group {
	final static long fillOff = -10_000_000;

	final Label title;
	final Canvas hitBox;
	final Canvas hitBoxBackgrounds[];
	private long[] fill = new long[] {fillOff, fillOff};

	public VisualColumn(DeviceSetDef_0_0_1.ColumnDef columnDef) {
		title = new Label();
		title.textProperty().bind(columnDef.name);
		title.setWrapText(true);
		title.setAlignment(Pos.CENTER);
		hitBox = new Canvas();
		hitBoxBackgrounds = new Canvas[] {
				new Canvas(), new Canvas()
		};
		getChildren().addAll(title, hitBoxBackgrounds[0], hitBoxBackgrounds[1], hitBox);
	}

	public void resize(Context context) {
		double width = context.columnWidth();
		double radius = context.eventRadius();
		double lineWidth = 1.5;
		double useRadius = radius - 4;
		{
			hitBox.setWidth(width);
			hitBox.setHeight(radius * 2 + lineWidth * 2);
			GraphicsContext gc = hitBox.getGraphicsContext2D();
			gc.clearRect(0, 0, hitBox.getWidth() + 1, hitBox.getHeight() + 1);
			gc.beginPath();
			gc.setStroke(Color.BLACK);
			gc.setLineWidth(lineWidth);
			gc.arc(radius, radius, useRadius, useRadius, 90, 180);
			gc.arc(width - radius, radius, useRadius, useRadius, 270, 180);
			gc.arc(2 * radius, radius, useRadius, useRadius, 90, 180);
			gc.arc(width - 2 * radius, radius, useRadius, useRadius, 270, 180);
			gc.closePath();
			gc.stroke();
		}
		for (Pair<Integer, Color> fill : iterable(enumerate(Stream.of(Color.rgb(87, 228, 12),
				Color.rgb(173, 11, 56)
		)))) {
			Canvas bg = hitBoxBackgrounds[fill.first];
			bg.setWidth(width);
			bg.setHeight(radius * 2 + lineWidth * 2);
			GraphicsContext gc = bg.getGraphicsContext2D();
			gc.clearRect(0, 0, bg.getWidth() + 1, bg.getHeight() + 1);
			gc.beginPath();
			gc.setFill(fill.second);
			gc.beginPath();
			gc.arc(radius, radius, useRadius, useRadius, 90, 180);
			gc.arc(2 * radius, radius, useRadius, useRadius, 270, -180);
			gc.closePath();
			gc.fill();
			gc.beginPath();
			gc.arc(width - radius, radius, useRadius, useRadius, 270, 180);
			gc.arc(width - 2 * radius, radius, useRadius, useRadius, 90, -180);
			gc.closePath();
			gc.fill();
		}

		title.setAlignment(Pos.BASELINE_CENTER);
		title.setMinSize(width, 40);
		title.setLayoutY(context.height - 48);
		title.setMaxWidth(width);
		double hitBoxY = context.msToPixel((long) (context.profile.windowTimeBefore * context.floatSpeed)) - radius;
		hitBox.setLayoutY(hitBoxY);
		hitBoxBackgrounds[0].setLayoutY(hitBoxY);
		hitBoxBackgrounds[0].opacityProperty().set(0);
		hitBoxBackgrounds[1].setLayoutY(hitBoxY);
		hitBoxBackgrounds[1].opacityProperty().set(0);
	}

	public void updateFill() {
		long now = Instant.now().toEpochMilli();
		final long cutoff = 200;
		for (int i = 0; i < fill.length; ++i) {
			long fv = fill[i];
			long diff = now - fv;
			if (diff > cutoff + 1000) // Don't mess with opacity too much in case there's recalculation
				continue;
			double opacity = Math.pow(Math.sin(Math.max(0, 1.0 - diff / (double) cutoff) * Math.PI / 2), 2);
			hitBoxBackgrounds[i].opacityProperty().set(opacity);
		}
	}

	public void setHit() {
		fill[0] = Instant.now().toEpochMilli();
		fill[1] = fillOff;
		hitBoxBackgrounds[1].opacityProperty().set(0);
	}

	public void setMiss() {
		fill[1] = Instant.now().toEpochMilli();
		fill[0] = fillOff;
		hitBoxBackgrounds[0].opacityProperty().set(0);
	}
}
