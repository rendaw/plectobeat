package com.zarbosoft.plectobeat.visual;

import com.google.common.math.DoubleMath;
import com.zarbosoft.plectobeat.ColumnEvent;
import com.zarbosoft.plectobeat.Context;
import com.zarbosoft.plectobeat.HitState;
import com.zarbosoft.plectobeat.config.deviceset.DeviceSetDef_0_0_1;
import javafx.scene.Group;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;

public class VisualEvent extends Group {
	Canvas outer = new Canvas();
	Canvas inner = new Canvas();
	double half;
	double width;
	double height;

	public VisualEvent() {
		getChildren().addAll(inner, outer);
	}

	public void set(Context context, DeviceSetDef_0_0_1.ColumnDef column, long now, ColumnEvent event) {
		setVisible(true);
		final double minSizePadding = 100;
		double standardHalf = context.eventRadius();
		double expectedHalf =
				standardHalf * Math.min(2.0, ((event.velocity + minSizePadding) / (80.0 + minSizePadding)));
		double expectedWidth = standardHalf * 4;
		double expectedHeight =
				(column.matchNoteEnd ? context.msToPixel((event.end - event.start) / 1000) : 0) + expectedHalf * 2;
		if (!DoubleMath.fuzzyEquals(expectedHalf, half, 1) ||
				!DoubleMath.fuzzyEquals(expectedWidth, width, 1) ||
				!DoubleMath.fuzzyEquals(expectedHeight, height, 1)) {
			half = expectedHalf;
			width = expectedWidth;
			height = expectedHeight;
			double middle = expectedWidth / 2;
			double radius = half - 4;
			{
				inner.setWidth(width);
				inner.setHeight(height);
				GraphicsContext gc = inner.getGraphicsContext2D();
				gc.clearRect(0, 0, inner.getWidth() + 1, inner.getHeight() + 1);
				gc.setFill(column.fillColor.get());
				gc.beginPath();
				gc.arc(middle, half, radius, radius, 180, -180);
				gc.arc(middle, height - half, radius, radius, 0, -180);
				gc.closePath();
				gc.fill();
			}
			{
				outer.setWidth(width);
				outer.setHeight(height);
				GraphicsContext gc = outer.getGraphicsContext2D();
				gc.clearRect(0, 0, outer.getWidth() + 1, outer.getHeight() + 1);
				gc.beginPath();
				gc.setLineWidth(2);
				gc.setStroke(column.outlineColor.get());
				gc.arc(middle, half, radius, radius, 180, -180);
				gc.arc(middle, height - half, radius, radius, 0, -180);
				gc.closePath();
				gc.stroke();
			}
		}
		long timeDist = (event.start - now) / 1000;
		double y = context.msToPixel((long) (context.profile.windowTimeBefore * context.floatSpeed) - timeDist) -
				expectedHeight + half;
		setLayoutY(y);
		inner.opacityProperty().set(event.startHit == HitState.HIT || event.endHit == HitState.HIT ? 0 : 1);
	}
}
