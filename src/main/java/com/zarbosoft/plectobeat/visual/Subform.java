package com.zarbosoft.plectobeat.visual;

import javafx.scene.layout.*;
import javafx.scene.paint.Color;

public class Subform extends Form {
	public Subform() {
		setBorder(new Border(new BorderStroke(Color.GRAY,
				BorderStrokeStyle.SOLID,
				new CornerRadii(4),
				new BorderWidths(1)
		)));
	}
}
