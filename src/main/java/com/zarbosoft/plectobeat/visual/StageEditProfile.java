package com.zarbosoft.plectobeat.visual;

import com.zarbosoft.plectobeat.Context;
import com.zarbosoft.plectobeat.config.profile.ProfileDef_0_0_1;
import com.zarbosoft.rendaw.common.Assertion;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;

import static com.zarbosoft.plectobeat.Main.localization;

public class StageEditProfile extends BaseStage {
	public final ProfileDef_0_0_1 profile;
	private final Context context;

	public int createTrackDurationSubform(GridPane form, int formRow, ProfileDef_0_0_1.StartModeDelay startMode) {
		ComboBox<ProfileDef_0_0_1.StartModeDelay.PauseDurationMode> combo = new ComboBox<>();
		combo.getItems().addAll(ProfileDef_0_0_1.StartModeDelay.PauseDurationMode.values());
		combo.setCellFactory(view -> new ListCell<ProfileDef_0_0_1.StartModeDelay.PauseDurationMode>() {
			@Override
			protected void updateItem(ProfileDef_0_0_1.StartModeDelay.PauseDurationMode item, boolean empty) {
				super.updateItem(item, empty);
				if (item != null)
					switch (item) {
						case BAR:
							setText(localization.getString("bars"));
							break;
						case CLOCK:
							setText(localization.getString("clock"));
							break;
					}
			}
		});
		combo.setButtonCell(combo.getCellFactory().call(null));
		form.addRow(formRow++, new Label("• " + localization.getString("start.delay")), combo);

		VBox subforms = new VBox();
		form.add(subforms, 0, formRow++, 2, 1);
		combo.valueProperty().addListener((
				(observable, oldValue, newValue) -> {
					startMode.pauseDuration = newValue;
					subforms.getChildren().clear();
					switch (newValue) {
						case BAR: {
							GridPane subsubform = new Form();
							int subFormRow = 0;
							subsubform.addRow(subFormRow++,
									new Label(localization.getString("number.of.bars")),
									StageMain.numberEntry(startMode.pauseDurationBars.bars, value -> {
										startMode.pauseDurationBars.bars = value;
									})
							);
							subforms.getChildren().addAll(subsubform);
							break;
						}
						case CLOCK: {
							GridPane subsubform = new Form();
							int subFormRow = 0;
							subsubform.addRow(subFormRow++,
									new Label(localization.getString("milliseconds")),
									StageMain.nonlinearSlider(0,
											30000,
											startMode.pauseDurationClock.ms,
											value -> startMode.pauseDurationClock.ms = value
									)
							);
							subforms.getChildren().addAll(subsubform);
							break;
						}
					}
					sizeToScene();
				}
		));
		combo.setValue(startMode.pauseDuration);

		return formRow;
	}

	public GridPane createStartModeDelaySubform() {
		GridPane delaySubform = new Subform();
		int subFormRow = 0;
		subFormRow = createTrackDurationSubform(delaySubform, subFormRow, profile.startModeDelay);
		delaySubform.addRow(subFormRow++, new Label(localization.getString("extra.silence")), StageMain.nonlinearSlider(
				0,
				30000,
				profile.startModeDelay.extraDelay,
				value -> profile.startModeDelay.extraDelay = value
		));
		return delaySubform;
	}

	public GridPane createStartModeTapSubform() {
		GridPane tapSubform = new Subform();
		int subFormRow = 0;
		tapSubform.addRow(subFormRow++,
				new Label(localization.getString("taps")),
				StageMain.numberEntry(profile.startModeTap.taps,
						value -> profile.startModeTap.taps = (int) (long) value
				)
		);
		tapSubform.addRow(subFormRow++,
				new Label(localization.getString("timeout")),
				StageMain.nonlinearSlider(100,
						3000,
						(int) profile.startModeTap.maxTime,
						value -> profile.startModeTap.maxTime = value
				)
		);
		tapSubform.addRow(subFormRow++,
				new Label(localization.getString("set.tempo")),
				StageMain.checkBox(profile.startModeTap.setTempo, v -> profile.startModeTap.setTempo = v)
		);
		tapSubform.addRow(subFormRow++, new Label(localization.getString("set.bar.denominator")), StageMain.numberEntry(
				profile.startModeTap.beatDivisions,
				value -> profile.startModeTap.beatDivisions = (int) (long) value
		));
		return tapSubform;
	}

	public GridPane createMistakeModeIgnoreSubform() {
		GridPane subform = new Subform();
		return subform;
	}

	public GridPane createMistakeModeContinueSubform() {
		GridPane continueSubform = new Subform();
		int subFormRow = 0;
		continueSubform.addRow(subFormRow++, new Label(localization.getString("record.misses")), StageMain.checkBox(
				profile.mistakeModeContinue.recordMistakes,
				value -> profile.mistakeModeContinue.recordMistakes = value
		));
		return continueSubform;
	}

	public GridPane createMistakeModePauseSubform() {
		GridPane pauseSubform = new Subform();
		int subFormRow = 0;
		pauseSubform.addRow(subFormRow++, new Label(localization.getString("retry.note.ends")), StageMain.checkBox(
				profile.mistakeModePause.retryNoteEnds,
				value -> profile.mistakeModePause.retryNoteEnds = value
		));
		pauseSubform.addRow(subFormRow++,
				new Label(localization.getString("pause.ms.after.note")),
				StageMain.nonlinearSlider(0,
						1000,
						profile.mistakeModePause.pauseAfter,
						v -> profile.mistakeModePause.pauseAfter = v
				)
		);
		return pauseSubform;
	}

	public GridPane createMistakeModeRewindSubform() {
		GridPane rewindSubform = new Subform();
		int subFormRow = 0;
		rewindSubform.addRow(subFormRow++,
				new Label(localization.getString("misses")),
				StageMain.numberEntry(profile.mistakeModeRewind.mistakes,
						value -> profile.mistakeModeRewind.mistakes = (int) (long) value
				)
		);
		rewindSubform.addRow(subFormRow++,
				new Label(localization.getString("in.bars")),
				StageMain.numberEntry(profile.mistakeModeRewind.bars,
						value -> profile.mistakeModeRewind.bars = (int) (long) value
				)
		);
		rewindSubform.addRow(subFormRow++,
				new Label(localization.getString("retry.note.ends")),
				StageMain.checkBox(profile.mistakeModeRewind.includeNoteEnds,
						v -> profile.mistakeModeRewind.includeNoteEnds = v
				)
		);
		rewindSubform.addRow(subFormRow++,
				new Label(localization.getString("need.no.miss.retry")),
				StageMain.checkBox(profile.mistakeModeRewind.requirePerfect)
		);
		{
			ComboBox<ProfileDef_0_0_1.StartModeSelection> combo = new ComboBox<>();
			combo.getItems().addAll(ProfileDef_0_0_1.StartModeSelection.values());
			combo.setCellFactory(view -> new ListCell<ProfileDef_0_0_1.StartModeSelection>() {
				@Override
				protected void updateItem(ProfileDef_0_0_1.StartModeSelection item, boolean empty) {
					super.updateItem(item, empty);
					if (item != null)
						switch (item) {
							case DELAY:
								setText(localization.getString("delay"));
								break;
							case TAP:
								setText(localization.getString("tap"));
								break;
						}
				}
			});
			combo.setButtonCell(combo.getCellFactory().call(null));
			rewindSubform.addRow(subFormRow++, new Label(localization.getString("retry.start.mode")), combo);

			VBox startSubforms = new VBox();

			rewindSubform.add(startSubforms, 0, subFormRow++, 2, 1);

			combo.valueProperty().addListener((
					(observable, oldValue, newValue) -> {
						profile.mistakeModeRewind.startMode = newValue;
						startSubforms.getChildren().clear();
						switch (newValue) {
							case DELAY:
								startSubforms.getChildren().addAll(createStartModeDelaySubform());
								break;
							case TAP:
								startSubforms.getChildren().addAll(createStartModeTapSubform());
								break;
						}
						sizeToScene();
					}
			));
			combo.setValue(profile.mistakeModeRewind.startMode);
		}
		return rewindSubform;
	}

	StageEditProfile(Context context, ProfileDef_0_0_1 profile) {
		this.context = context;
		this.profile = profile;
		titleProperty().bind(profile.name);

		GridPane form = new Form();
		int formRow = 0;

		TextField name = new TextField();
		name.textProperty().bindBidirectional(profile.name);
		form.addRow(formRow++, new Label(localization.getString("name")), name);

		ComboBox<String> speedCombo = StageMain.speedCombo(value -> profile.normalSpeedPercent = value);
		form.addRow(formRow++, new Label(localization.getString("normal.speed")), speedCombo);

		form.addRow(formRow++,
				new Label(localization.getString("allow.hit.early.ms")),
				StageMain.nonlinearSlider(0, 1000, profile.detectMs, value -> profile.detectMs = value)
		);

		form.addRow(formRow++,
				new Label(localization.getString("show.time.before.ms")),
				StageMain.numberEntry(profile.windowTimeBefore, value -> {
					profile.windowTimeBefore = (int) (long) value;
					context.columns.forEach(column -> column.resize(context));
				})
		);

		form.addRow(formRow++,
				new Label(localization.getString("show.time.after.ms")),
				StageMain.numberEntry(profile.windowTimeAfter, value -> {
					profile.windowTimeAfter = (int) (long) value;
					context.columns.forEach(column -> column.resize(context));
				})
		);

		form.addRow(formRow++,
				new Label(localization.getString("default.device.set")),
				StageMain.deviceDropdown(context, value -> profile.deviceId = value.id)
		);

		form.add(new Separator(), 1, formRow++);

		{
			ComboBox<ProfileDef_0_0_1.StartModeSelection> combo = new ComboBox<>();
			combo.getItems().addAll(ProfileDef_0_0_1.StartModeSelection.values());
			combo.setCellFactory(view -> new ListCell<ProfileDef_0_0_1.StartModeSelection>() {
				@Override
				protected void updateItem(ProfileDef_0_0_1.StartModeSelection item, boolean empty) {
					super.updateItem(item, empty);
					if (item != null)
						switch (item) {
							case DELAY:
								setText(localization.getString("delay"));
								break;
							case TAP:
								setText(localization.getString("tap"));
								break;
						}
				}
			});
			combo.setButtonCell(combo.getCellFactory().call(null));
			form.addRow(formRow++, new Label(localization.getString("start.mode")), combo);

			VBox startSubforms = new VBox();

			form.add(startSubforms, 0, formRow++, 2, 1);

			combo.valueProperty().addListener((
					(observable, oldValue, newValue) -> {
						profile.startMode = newValue;
						startSubforms.getChildren().clear();
						switch (newValue) {
							case DELAY:
								startSubforms.getChildren().addAll(createStartModeDelaySubform());
								break;
							case TAP:
								startSubforms.getChildren().addAll(createStartModeTapSubform());
								break;
						}
						sizeToScene();
						if (context.profile == profile)
							context.startManager = profile.generateStartManager(context);
					}
			));
			combo.setValue(profile.startMode);
		}

		form.add(new Separator(), 1, formRow++);

		{
			ComboBox<ProfileDef_0_0_1.MistakeModeSelection> combo = new ComboBox<>();
			combo.getItems().addAll(ProfileDef_0_0_1.MistakeModeSelection.values());
			combo.setCellFactory(view -> new ListCell<ProfileDef_0_0_1.MistakeModeSelection>() {
				@Override
				protected void updateItem(ProfileDef_0_0_1.MistakeModeSelection item, boolean empty) {
					super.updateItem(item, empty);
					if (item != null)
						switch (item) {
							case IGNORE:
								setText(localization.getString("ignore"));
								break;
							case CONTINUE:
								setText(localization.getString("continue"));
								break;
							case PAUSE:
								setText(localization.getString("pause"));
								break;
							case REWIND:
								setText(localization.getString("rewind"));
								break;
						}
				}
			});
			combo.setButtonCell(combo.getCellFactory().call(null));
			form.addRow(formRow++, new Label(localization.getString("on.miss")), combo);

			VBox subforms = new VBox();

			form.add(subforms, 0, formRow++, 2, 1);

			combo.valueProperty().addListener((
					(observable, oldValue, newValue) -> {
						profile.mistakeMode = newValue;
						subforms.getChildren().clear();
						switch (newValue) {
							case IGNORE:
								subforms.getChildren().addAll(createMistakeModeIgnoreSubform());
								break;
							case CONTINUE:
								subforms.getChildren().addAll(createMistakeModeContinueSubform());
								break;
							case PAUSE:
								subforms.getChildren().addAll(createMistakeModePauseSubform());
								break;
							case REWIND:
								subforms.getChildren().addAll(createMistakeModeRewindSubform());
								break;
							default:
								throw new Assertion();
						}
						sizeToScene();
						if (context.profile == profile)
							context.mistakeManager = profile.generateMistakeManager();
					}
			));
			combo.setValue(profile.mistakeMode);
		}

		form.add(new Separator(), 1, formRow++);

		{
			ComboBox<ProfileDef_0_0_1.TrackEndMode> combo =
					new ComboBox<>(FXCollections.observableArrayList(ProfileDef_0_0_1.TrackEndMode.values()));
			combo.setCellFactory(v -> new ListCell<ProfileDef_0_0_1.TrackEndMode>() {
				@Override
				protected void updateItem(ProfileDef_0_0_1.TrackEndMode item, boolean empty) {
					super.updateItem(item, empty);
					if (item != null)
						switch (item) {
							case STOP:
								setText(localization.getString("stop"));
								break;
							case NEXT:
								setText(localization.getString("next.track"));
								break;
							case REWIND:
								setText(localization.getString("stop.and.rewind"));
								break;
							case REWIND_PLAY:
								setText(localization.getString("rewind.and.play"));
								break;
						}
				}
			});
			combo.setButtonCell(combo.getCellFactory().call(null));
			combo.getSelectionModel().select(profile.onTrackEnd);
			combo
					.getSelectionModel()
					.selectedItemProperty()
					.addListener((observable, oldValue, newValue) -> profile.onTrackEnd = newValue);
			form.addRow(formRow++, new Label(localization.getString("at.track.end")), combo);
		}

		{
			ComboBox<ProfileDef_0_0_1.PlaylistEndMode> combo =
					new ComboBox<>(FXCollections.observableArrayList(ProfileDef_0_0_1.PlaylistEndMode.values()));
			combo.setCellFactory(v -> new ListCell<ProfileDef_0_0_1.PlaylistEndMode>() {
				@Override
				protected void updateItem(ProfileDef_0_0_1.PlaylistEndMode item, boolean empty) {
					super.updateItem(item, empty);
					if (item != null)
						switch (item) {
							case STOP:
								setText(localization.getString("stop"));
								break;
							case LOOP_STOP:
								setText(localization.getString("stop.at.first.track"));
								break;
							case LOOP_PLAY:
								setText(localization.getString("play.from.first.track"));
								break;
						}
				}
			});
			combo.setButtonCell(combo.getCellFactory().call(null));
			combo.getSelectionModel().select(profile.onPlaylistEnd);
			combo
					.getSelectionModel()
					.selectedItemProperty()
					.addListener((observable, oldValue, newValue) -> profile.onPlaylistEnd = newValue);
			form.addRow(formRow++, new Label(localization.getString("at.playlist.end")), combo);
		}

		ColumnConstraints leftConstraints = new ColumnConstraints();
		ColumnConstraints rightConstraints = new ColumnConstraints();
		rightConstraints.setHgrow(Priority.ALWAYS);
		form.getColumnConstraints().addAll(leftConstraints, rightConstraints);

		Button close = new Button(localization.getString("close"));
		close.setOnAction(e -> {
			close();
			context.stageEditProfile = null;
		});
		HBox closeBox = new HBox();
		closeBox.setAlignment(Pos.CENTER_RIGHT);
		closeBox.setPadding(new Insets(4, 4, 4, 4));
		closeBox.getChildren().addAll(close);

		VBox topLayout = new VBox();
		topLayout.getChildren().addAll(form, closeBox);

		setScene(new Scene(topLayout));
		show();
	}

	@Override
	public void close() {
		super.close();
		context.stageEditProfile = null;
		context.save();
	}
}
