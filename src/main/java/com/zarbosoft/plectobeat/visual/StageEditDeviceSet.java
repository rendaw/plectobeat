package com.zarbosoft.plectobeat.visual;

import com.zarbosoft.plectobeat.Context;
import com.zarbosoft.plectobeat.Main;
import com.zarbosoft.plectobeat.MidiHandle;
import com.zarbosoft.plectobeat.config.deviceset.DeviceSetDef_0_0_1;
import com.zarbosoft.rendaw.common.Assertion;
import com.zarbosoft.rendaw.common.DeadCode;
import com.zarbosoft.rendaw.common.Pair;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableBooleanValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import javax.sound.midi.MidiDevice;
import java.util.*;
import java.util.stream.Collectors;

import static com.zarbosoft.plectobeat.Main.localization;
import static com.zarbosoft.plectobeat.visual.StageMain.*;
import static com.zarbosoft.rendaw.common.Common.stamp;

public class StageEditDeviceSet extends BaseStage {
	public final TimerTask midiClearTask;
	public final List<MidiHandle> openedDevices = new ArrayList<>();
	public final DeviceSetDef_0_0_1 device;
	private final Context context;
	private CalibrateColumn calibrationStage;

	public StageEditDeviceSet(Context context, DeviceSetDef_0_0_1 deviceSetDef) {
		this.context = context;
		device = deviceSetDef;
		titleProperty().bind(deviceSetDef.name);

		GridPane form = new Form();
		int formRow = 0;

		TextField name = new TextField();
		name.textProperty().bindBidirectional(deviceSetDef.name);
		form.addRow(formRow++, new Label(localization.getString("name")), name);

		Tab mainTab = new Tab(localization.getString("top"));
		mainTab.setClosable(false);
		mainTab.setContent(form);

		Tab columnsTab = new Tab(localization.getString("columns"));
		columnsTab.setClosable(false);
		columnsTab.setContent(new Builder.ListBuilder<DeviceSetDef_0_0_1.ColumnDef>(deviceSetDef.columns,
				() -> new ListCell<DeviceSetDef_0_0_1.ColumnDef>() {
					@Override
					protected void updateItem(DeviceSetDef_0_0_1.ColumnDef item, boolean empty) {
						super.updateItem(item, empty);
						if (item == null) {
							textProperty().unbind();
							styleProperty().unbind();
						} else {
							textProperty().bind(item.name);
							styleProperty().bind(Bindings.createStringBinding(() -> {
								return item.enabled.get() ? "-fx-text-fill: black" : "-fx-text-fill: grey";
							}, item.enabled));
						}
					}
				}
		).addNewDelete(() -> DeviceSetDef_0_0_1.ColumnDef.createDefault(), d -> context.loadMidi()).addButton(list -> {
			Button edit = new Button(localization.getString("edit"));
			edit.setOnAction(e -> {
				DeviceSetDef_0_0_1.ColumnDef selected = list.getSelectionModel().getSelectedItem();
				if (selected == null)
					return;
				new EditColumnStage(selected, device, this);
				context.loadMidi();
			});
			return edit;
		}).addButton(list -> {
			Button toggle = new Button(localization.getString("toggle"), StageMain.icon("eye-off.svg"));
			toggle.setOnAction(e -> {
				list.getSelectionModel().getSelectedItems().forEach(c -> c.enabled.setValue(!c.enabled.get()));
				context.loadMidi();
			});
			return toggle;
		}).addButton(list -> {
			Button button = new Button(localization.getString("calibrate"));
			button.setOnAction(e -> {
				for (DeviceSetDef_0_0_1.ColumnDef column : list.getSelectionModel().getSelectedItems()) {
					new CalibrateColumn(column, this);
				}
			});
			return button;
		}).moveButtons(() -> context.loadMidi()).build());

		Tab devicesTab = new Tab(localization.getString("devices"));
		devicesTab.setClosable(false);
		{
			Map<Integer, Pair<Long, SimpleBooleanProperty>> deviceLightTimer = new HashMap<>();
			Main.timer.scheduleAtFixedRate(midiClearTask = new TimerTask() {
				@Override
				public void run() {
					Platform.runLater(() -> {
						long now = stamp();
						ArrayList<Map.Entry<Integer, Pair<Long, SimpleBooleanProperty>>> walk =
								new ArrayList<>(deviceLightTimer.entrySet());
						for (Map.Entry<Integer, Pair<Long, SimpleBooleanProperty>> e : walk) {
							if (e.getValue().first < now) {
								e.getValue().second.set(false);
								deviceLightTimer.remove(e.getKey());
							}
						}
					});
				}
			}, 0L, 10L);
			Map<Integer, SimpleBooleanProperty> midiActivity = new HashMap<>();
			List<MidiDevice.Info> useInfo = new ArrayList<>();
			List<MenuItem> newDeviceMenu = new ArrayList<>();
			DeviceSetDef_0_0_1.DeviceDef.midiDevices().forEach(info -> {
				SimpleBooleanProperty property = new SimpleBooleanProperty(false);
				MidiHandle handle;
				try {
					handle = new MidiHandle(info) {
						@Override
						public void accept(int numericId, int channel, int note, int velocity, boolean on) {
							Platform.runLater(() -> {
								property.set(true);
								deviceLightTimer.put(numericId, new Pair<>(stamp() + 500, property));
								if (on && calibrationStage != null) {
									Optional<DeviceSetDef_0_0_1.DeviceDef> found =
											device.devices.stream().filter(d -> d.numericId == numericId).findFirst();
									if (found.isPresent())
										calibrationStage.setNote(found.get().name.get(), found.get().id, channel, note);
								}
							});
						}
					};
				} catch (MidiHandle.MidiOpenFailed e) {
					Main.logger.warn("Failed to open device for device settings", e);
					return;
				}
				openedDevices.add(handle);
				midiActivity.put(DeviceSetDef_0_0_1.DeviceDef.numericHash(info), property);
				useInfo.add(info);
			});
			devicesTab.setContent(new Builder.ListBuilder<DeviceSetDef_0_0_1.DeviceDef>(deviceSetDef.devices,
					() -> new ListCell<DeviceSetDef_0_0_1.DeviceDef>() {
						@Override
						protected void updateItem(DeviceSetDef_0_0_1.DeviceDef item, boolean empty) {
							super.updateItem(item, empty);
							if (item == null) {
								textProperty().unbind();
								graphicProperty().unbind();
							} else {
								textProperty().bind(item.name);
								ObservableBooleanValue property = midiActivity.get(item.numericId);
								if (property == null)
									graphicProperty().setValue(StageMain.icon("circle-medium.svg"));
								else
									graphicProperty().bind(Bindings.<Node>createObjectBinding(() -> midiActivity
											.get(item.numericId)
											.get() ?
											StageMain.icon("circle-slice-8.svg") :
											StageMain.icon("circle-outline.svg"), property));
							}
						}
					}
			).addButton(list -> {
				MenuButton new_ =
						new MenuButton(localization.getString("new"), StageMain.icon("plus-circle-outline.svg"));
				new_.setMaxWidth(150);
				useInfo.forEach(info -> {
					MenuItem menuItem;
					new_.getItems().add(menuItem = new MenuItem(info.getName()));
					menuItem.setOnAction(e -> {
						DeviceSetDef_0_0_1.DeviceDef created;
						list.getItems().add(created = DeviceSetDef_0_0_1.DeviceDef.createFromInfo(info));
						list.getSelectionModel().select(created);
						if (device == context.device)
							context.setDevice(device);
					});
				});
				{
					MenuItem menuItem;
					new_.getItems().add(menuItem = new MenuItem(localization.getString("other")));
					menuItem.setOnAction(e -> {
						DeviceSetDef_0_0_1.DeviceDef created = new DeviceSetDef_0_0_1.DeviceDef();
						created.name.set(localization.getString("new.device"));
						created.midiName = "";
						created.midiVendor = "";
						created.finish();
						list.getItems().add(created);
						list.getSelectionModel().select(created);
						if (device == context.device)
							context.setDevice(device);
					});
				}
				new_.getItems().addAll(newDeviceMenu);
				return new_;
			}).addButton(list -> {
				Button remove =
						new Button(localization.getString("delete"), StageMain.icon("minus-circle-outline.svg"));
				remove.setOnAction(e -> {
					deviceSetDef.devices.removeAll(list.getSelectionModel().getSelectedItems());
					if (device == context.device)
						context.setDevice(device);
				});
				return remove;
			}).addButton(list -> {
				Button edit = new Button(localization.getString("edit"));
				edit.setOnAction(e -> {
					DeviceSetDef_0_0_1.DeviceDef selected = list.getSelectionModel().getSelectedItem();
					if (selected == null)
						return;
					new EditDeviceStage(selected, this);
					selected.reId();
					if (device == context.device)
						context.setDevice(device);
				});
				return edit;
			}).build());
		}

		TabPane tabs = new TabPane();
		tabs.getTabs().addAll(mainTab, columnsTab, devicesTab);

		Button close = new Button(localization.getString("close"));
		close.setOnAction(e -> {
			close();
			context.stageEditDeviceSet = null;
		});
		HBox closeBox = new HBox();
		closeBox.setPadding(new Insets(4, 4, 4, 4));
		closeBox.setAlignment(Pos.CENTER_RIGHT);
		closeBox.getChildren().add(close);

		VBox topLayout = new VBox();
		topLayout.getChildren().addAll(tabs, closeBox);

		setScene(new Scene(topLayout));
		show();
	}

	@Override
	public void close() {
		super.close();
		midiClearTask.cancel();
		openedDevices.forEach(d -> d.close());
		context.stageEditDeviceSet = null;
		context.save();
	}

	public static class EditDeviceStage extends BaseStage {
		EditDeviceStage(DeviceSetDef_0_0_1.DeviceDef device, Stage modalParent) {
			Form form = new Form();
			int formRow = 0;

			form.addRow(formRow++, new Label(localization.getString("name")), textEntry(device.name));
			form.addRow(formRow++,
					new Label(localization.getString("midi.name")),
					textEntry(device.midiName, v -> device.midiName = v)
			);
			form.addRow(formRow++,
					new Label(localization.getString("midi.vendor")),
					textEntry(device.midiVendor, v -> device.midiVendor = v)
			);

			Button close = new Button(localization.getString("close"));
			close.setOnAction(e -> {
				close();
			});
			titleProperty().bind(device.name);
			HBox closeBox = new HBox();
			closeBox.setPadding(new Insets(4, 4, 4, 4));
			closeBox.setAlignment(Pos.CENTER_RIGHT);
			closeBox.getChildren().add(close);

			VBox topLayout = new VBox();
			topLayout.getChildren().addAll(form, closeBox);

			setScene(new Scene(topLayout));

			initOwner(modalParent);
			initModality(Modality.APPLICATION_MODAL);
			showAndWait();
		}
	}

	public static class EditColumnStage extends BaseStage {
		EditColumnStage(DeviceSetDef_0_0_1.ColumnDef column, DeviceSetDef_0_0_1 deviceSet, Stage modalParent) {
			Tab mainTab = new Tab(localization.getString("top"));
			mainTab.setClosable(false);
			{
				Form form = new Form();
				int formRow = 0;
				form.addRow(formRow++, new Label(localization.getString("name")), textEntry(column.name));
				form.addRow(formRow++, new Label(localization.getString("enabled")), checkBox(column.enabled));
				form.addRow(formRow++,
						new Label(localization.getString("outline")),
						colorEntry(column.outlineColor, v -> column.outlineColor = v)
				);
				form.addRow(formRow++,
						new Label(localization.getString("fill")),
						colorEntry(column.fillColor, v -> column.fillColor = v)
				);
				form.addRow(formRow++,
						new Label(localization.getString("detect.ms")),
						nonlinearSlider(0, 3000, column.detectMs, v -> column.detectMs = v)
				);
				form.addRow(formRow++,
						new Label(localization.getString("match.note.end")),
						checkBox(column.matchNoteEnd, v -> column.matchNoteEnd = v)
				);
				mainTab.setContent(form);
			}

			Tab notesTab = new Tab(localization.getString("notes"));
			notesTab.setClosable(false);
			notesTab.setContent(new Builder.ListBuilder<DeviceSetDef_0_0_1.ColumnSource>(column.sources,
					() -> new ListCell<DeviceSetDef_0_0_1.ColumnSource>() {
						@Override
						protected void updateItem(DeviceSetDef_0_0_1.ColumnSource item, boolean empty) {
							super.updateItem(item, empty);
							if (item == null) {
								textProperty().unbind();
							} else {
								textProperty().bind(Bindings.createStringBinding(() -> String.format(localization.getString(
										"tr.s.ch.s.note.s"),
										item.track.get(),
										item.channel.get(),
										item.note.get()
								), item.track, item.channel, item.note));
							}
						}
					}
			).addNewDelete(() -> {
				DeviceSetDef_0_0_1.ColumnSource out = new DeviceSetDef_0_0_1.ColumnSource();
				out.track.set(-1);
				out.note.set(-1);
				out.channel.set(-1);
				return out;
			}, s -> {
			}).addButton(list -> {
				Button edit = new Button(localization.getString("edit"));
				edit.setOnAction(e -> {
					DeviceSetDef_0_0_1.ColumnSource selected = list.getSelectionModel().getSelectedItem();
					if (selected == null)
						return;
					new EditColumnSource(selected, this);
				});
				return edit;
			}).build());

			Tab triggersTab = new Tab(localization.getString("triggers"));
			triggersTab.setClosable(false);
			triggersTab.setContent(new Builder.ListBuilder<DeviceSetDef_0_0_1.ColumnTrigger>(column.triggers,
					() -> new ListCell<DeviceSetDef_0_0_1.ColumnTrigger>() {
						@Override
						protected void updateItem(DeviceSetDef_0_0_1.ColumnTrigger item, boolean empty) {
							super.updateItem(item, empty);
							if (item == null) {
								textProperty().unbind();
							} else {
								if (item instanceof DeviceSetDef_0_0_1.MidiTrigger) {
									textProperty().bind(Bindings.createStringBinding(() -> String.format(localization.getString(
											"dev.s.ch.s.note.s"),
											deviceSet.devices
													.stream()
													.filter(d -> d.numericId ==
															((DeviceSetDef_0_0_1.MidiTrigger) item).numericDeviceId)
													.findFirst()
													.map(d -> d.name.get())
													.orElse(((DeviceSetDef_0_0_1.MidiTrigger) item).deviceId
															.get()
															.isEmpty() ?
															localization.getString("any") :
															((DeviceSetDef_0_0_1.MidiTrigger) item).deviceId.get()),
											((DeviceSetDef_0_0_1.MidiTrigger) item).channel.get(),
											((DeviceSetDef_0_0_1.MidiTrigger) item).note.get()
											),
											((DeviceSetDef_0_0_1.MidiTrigger) item).deviceId,
											((DeviceSetDef_0_0_1.MidiTrigger) item).channel,
											((DeviceSetDef_0_0_1.MidiTrigger) item).note
									));
								} else
									throw new Assertion("TODO");
							}
						}
					}
			).addNewDelete(() -> {
				DeviceSetDef_0_0_1.MidiTrigger out = new DeviceSetDef_0_0_1.MidiTrigger();
				out.deviceId.set("");
				out.channel.set(-1);
				out.note.set(-1);
				return out;
			}, i -> {
			}).addButton(list -> {
				Button edit = new Button(localization.getString("edit"));
				edit.setOnAction(e -> {
					DeviceSetDef_0_0_1.ColumnTrigger selected = list.getSelectionModel().getSelectedItem();
					if (selected == null)
						return;
					if (selected instanceof DeviceSetDef_0_0_1.MidiTrigger)
						new EditMidiTrigger((DeviceSetDef_0_0_1.MidiTrigger) selected, deviceSet, this);
					else
						return;
				});
				return edit;
			}).build());

			TabPane tabs = new TabPane();
			tabs.getTabs().addAll(mainTab, notesTab, triggersTab);

			Button close = new Button(localization.getString("close"));
			close.setOnAction(e -> {
				close();
			});
			titleProperty().bind(column.name);
			HBox closeBox = new HBox();
			closeBox.setPadding(new Insets(4, 4, 4, 4));
			closeBox.setAlignment(Pos.CENTER_RIGHT);
			closeBox.getChildren().add(close);

			VBox topLayout = new VBox();
			topLayout.getChildren().addAll(tabs, closeBox);

			setScene(new Scene(topLayout));

			initOwner(modalParent);
			initModality(Modality.APPLICATION_MODAL);
			showAndWait();
		}
	}

	public static class EditMidiTrigger extends BaseStage {
		EditMidiTrigger(DeviceSetDef_0_0_1.MidiTrigger trigger, DeviceSetDef_0_0_1 deviceSet, Stage modalParent) {
			Form form = new Form();
			int formRow = 0;

			ComboBox<Pair<String, String>> devices = new ComboBox<>();
			devices
					.getItems()
					.addAll(deviceSet.devices
							.stream()
							.map(d -> new Pair<>(d.name.get(), d.id))
							.collect(Collectors.toList()));
			devices.getItems().add(new Pair<>(localization.getString("any"), ""));
			if (!devices.getItems().stream().anyMatch(p -> p.second.equals(trigger.deviceId))) {
				devices.getItems().add(new Pair<>(trigger.deviceId.get(), trigger.deviceId.get()));
			}
			devices
					.getItems()
					.stream()
					.filter(p -> p.second.equals(trigger.deviceId))
					.findFirst()
					.ifPresent(p -> devices.getSelectionModel().select(p));
			devices.setCellFactory(v -> new ListCell<Pair<String, String>>() {
				@Override
				protected void updateItem(Pair<String, String> item, boolean empty) {
					super.updateItem(item, empty);
					if (item == null)
						setText("");
					else
						setText(item.first);
				}
			});
			devices.setButtonCell(devices.getCellFactory().call(null));
			devices.getSelectionModel().selectedItemProperty().addListener((
					(observable, oldValue, newValue) -> {
						if (newValue == null)
							return;
						trigger.deviceId.set(newValue.second);
					}
			));
			form.addRow(formRow++, new Label(localization.getString("midi.device")), devices);
			form.addRow(formRow++, new Label(localization.getString("midi.channel")), numberEntry(trigger.channel));
			form.addRow(formRow++, new Label(localization.getString("midi.note")), numberEntry(trigger.note));

			Button close = new Button(localization.getString("close"));
			close.setOnAction(e -> {
				close();
			});
			setTitle(localization.getString("edit.midi.trigger"));
			HBox closeBox = new HBox();
			closeBox.setPadding(new Insets(4, 4, 4, 4));
			closeBox.setAlignment(Pos.CENTER_RIGHT);
			closeBox.getChildren().add(close);

			VBox topLayout = new VBox();
			topLayout.getChildren().addAll(form, closeBox);

			setScene(new Scene(topLayout));

			initOwner(modalParent);
			initModality(Modality.APPLICATION_MODAL);
			showAndWait();
		}
	}

	public static class EditColumnSource extends BaseStage {
		EditColumnSource(DeviceSetDef_0_0_1.ColumnSource source, Stage modalParent) {
			Form form = new Form();
			int formRow = 0;

			form.addRow(formRow++, new Label(localization.getString("midi.track")), numberEntry(source.track));
			form.addRow(formRow++, new Label(localization.getString("midi.channel")), numberEntry(source.channel));
			form.addRow(formRow++, new Label(localization.getString("midi.note")), numberEntry(source.note));

			Button close = new Button(localization.getString("close"));
			close.setOnAction(e -> {
				close();
			});
			setTitle(localization.getString("edit.note.source"));
			HBox closeBox = new HBox();
			closeBox.setPadding(new Insets(4, 4, 4, 4));
			closeBox.setAlignment(Pos.CENTER_RIGHT);
			closeBox.getChildren().add(close);

			VBox topLayout = new VBox();
			topLayout.getChildren().addAll(form, closeBox);

			setScene(new Scene(topLayout));

			initOwner(modalParent);
			initModality(Modality.APPLICATION_MODAL);
			showAndWait();
		}
	}

	public class CalibrateColumn extends BaseStage {
		DeviceSetDef_0_0_1.MidiTrigger out;
		SimpleBooleanProperty anyChannel = new SimpleBooleanProperty();
		Label deviceLabel = new Label();
		Label channelLabel = new Label();
		Label noteLabel = new Label();

		CalibrateColumn(DeviceSetDef_0_0_1.ColumnDef column, Stage modalParent) {
			setTitle(String.format(localization.getString("calibrate.s"), column.name));
			Form form = new Form();
			int formRow = 0;

			out = column.triggers
					.stream()
					.filter(t -> t instanceof DeviceSetDef_0_0_1.MidiTrigger)
					.map(t -> (DeviceSetDef_0_0_1.MidiTrigger) t)
					.findFirst()
					.orElseGet(() -> {
						DeviceSetDef_0_0_1.MidiTrigger out = new DeviceSetDef_0_0_1.MidiTrigger();
						out.note.set(60);
						out.channel.set(-1);
						out.deviceId.set("");
						return out;
					})
					.cloneFull();
			anyChannel.set(out.channel.get() == -1);
			anyChannel.addListener((observable, oldValue, newValue) -> updateChannelLabel());
			setNote(device.devices
							.stream()
							.filter(d -> d.numericId == out.numericDeviceId)
							.findFirst()
							.map(d -> d.midiName)
							.orElse(localization.getString("any.device")),
					out.deviceId.get(),
					out.channel.get(),
					out.note.get()
			);

			form.add(new Label(localization.getString("trigger.your.midi.device")), 0, formRow++, 2, 1);
			form.addRow(formRow++, new Label(localization.getString("any.channel")), checkBox(anyChannel));
			form.addRow(formRow++, new Label(localization.getString("device")), deviceLabel);
			form.addRow(formRow++, new Label(localization.getString("midi.channel")), channelLabel);
			form.addRow(formRow++, new Label(localization.getString("midi.note")), noteLabel);

			Button set = new Button(localization.getString("set"));
			set.setOnAction(e -> {
				column.triggers.clear();
				column.triggers.add(out);
				close();
			});
			Button close = new Button(localization.getString("close"));
			close.setOnAction(e -> {
				close();
			});
			HBox closeBox = new HBox();
			closeBox.setSpacing(8);
			closeBox.setPadding(new Insets(4, 4, 4, 4));
			closeBox.setAlignment(Pos.CENTER_RIGHT);
			closeBox.getChildren().addAll(close, set);

			VBox topLayout = new VBox();
			topLayout.getChildren().addAll(form, closeBox);

			setScene(new Scene(topLayout));

			initOwner(modalParent);
			initModality(Modality.APPLICATION_MODAL);
			calibrationStage = this;
			showAndWait();
			calibrationStage = null;
		}

		public void updateChannelLabel() {
			if (anyChannel.get()) {
				channelLabel.setText(localization.getString("any.channel"));
			} else {
				channelLabel.setText(String.format("%s (%s)", out.channel.get(), out.channel.get() + 1));
			}
		}

		public void setNote(String device, String deviceId, int channel, int note) {
			deviceLabel.setText(device);
			out.deviceId.setValue(deviceId);
			out.channel.setValue(channel);
			updateChannelLabel();
			out.note.setValue(note);
			String noteName;
			switch (note % 12) {
				case 0:
					noteName = "C";
					break;
				case 1:
					noteName = "C#";
					break;
				case 2:
					noteName = "D";
					break;
				case 3:
					noteName = "D#";
					break;
				case 4:
					noteName = "E";
					break;
				case 5:
					noteName = "F";
					break;
				case 6:
					noteName = "F#";
					break;
				case 7:
					noteName = "G";
					break;
				case 8:
					noteName = "G#";
					break;
				case 9:
					noteName = "A";
					break;
				case 10:
					noteName = "A#";
					break;
				case 11:
					noteName = "B";
					break;
				default:
					throw new DeadCode();
			}
			noteLabel.setText(String.format("%s (%s-%s)", note, noteName, (note / 12) + 1));
		}
	}
}
