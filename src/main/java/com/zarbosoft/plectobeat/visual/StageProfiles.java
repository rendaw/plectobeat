package com.zarbosoft.plectobeat.visual;

import com.zarbosoft.plectobeat.Context;
import com.zarbosoft.plectobeat.config.profile.ProfileDef_0_0_1;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

import static com.zarbosoft.plectobeat.Main.localization;

public class StageProfiles extends BaseStage {
	private final Context context;

	public StageProfiles(Context context) {
		this.context = context;
		setTitle(localization.getString("manage.profiles"));

		setScene(new Scene(new Builder.ListBuilder<ProfileDef_0_0_1>(context.profiles,
				() -> new ListCell<ProfileDef_0_0_1>() {
					@Override
					protected void updateItem(ProfileDef_0_0_1 item, boolean empty) {
						super.updateItem(item, empty);
						if (item == null)
							textProperty().unbind();
						else
							textProperty().bind(item.name);
					}
				}
		).addNewDelete(ProfileDef_0_0_1::new, null).addDuplicate(p -> {
			ProfileDef_0_0_1 out = p.cloneFull();
			out.name.set(String.format(localization.getString("s.copy"), out.name.get()));
			return out;
		}).addButton(list -> {
			Button edit = new Button(localization.getString("edit"));
			edit.setMaxWidth(150);
			edit.setOnAction(e -> {
				ProfileDef_0_0_1 selected = list.getSelectionModel().getSelectedItem();
				if (selected == null)
					return;
				if (context.stageEditProfile != null)
					if (context.stageEditProfile.profile == selected) {
						context.stageEditProfile.requestFocus();
						return;
					} else {
						context.stageEditProfile.close();
					}
				context.stageEditProfile = new StageEditProfile(context, selected);
			});
			return edit;
		}).addButton(list -> {
			Pane buttonSeparator = new Pane();
			VBox.setVgrow(buttonSeparator, Priority.ALWAYS);
			return buttonSeparator;
		}).addButton(list -> {
			Button close = new Button(localization.getString("close"));
			close.setMaxWidth(150);
			close.setOnAction(e -> {
				close();
				context.stageProfiles = null;
			});
			return close;
		}).select(context.profile).build()));

		show();
	}

	@Override
	public void close() {
		super.close();
		context.stageProfiles = null;
		context.save();
	}
}
