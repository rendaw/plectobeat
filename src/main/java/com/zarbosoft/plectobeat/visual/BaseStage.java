package com.zarbosoft.plectobeat.visual;

import com.zarbosoft.plectobeat.Main;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class BaseStage extends Stage {
	public static Image appIcon = new Image(Main.class.getResourceAsStream("icons/appicon.png"));

	public BaseStage() {
		getIcons().add(appIcon);
	}
}
