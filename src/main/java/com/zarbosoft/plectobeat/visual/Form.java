package com.zarbosoft.plectobeat.visual;

import javafx.geometry.Insets;
import javafx.scene.layout.GridPane;

public class Form extends GridPane {
	public Form() {
		setHgap(4);
		setVgap(8);
		setPadding(new Insets(4, 4, 4, 4));
	}
}
