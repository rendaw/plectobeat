package com.zarbosoft.plectobeat.visual;

import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static com.zarbosoft.plectobeat.Main.localization;

public class Builder {
	public static class ListBuilder<T> {
		private final ListView<T> list;
		private final VBox buttons;

		public ListBuilder(ObservableList<T> data, Supplier<ListCell<T>> listCellSupplier) {
			this.list = new ListView<>(data);
			list.setCellFactory(v -> listCellSupplier.get());
			list.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
			if (!data.isEmpty())
				list.getSelectionModel().select(data.get(0));
			buttons = new VBox();
			buttons.setPadding(new Insets(4, 4, 4, 4));
			buttons.setSpacing(8);
		}

		public ListBuilder<T> addNewDelete(Supplier<T> supplier, Consumer<List<T>> onDelete) {
			Button new_ = new Button(localization.getString("new"), StageMain.icon("plus-circle-outline.svg"));
			new_.setMaxWidth(150);
			new_.setOnAction(e -> {
				T out = supplier.get();
				list.getItems().add(out);
				list.getSelectionModel().clearSelection();
				list.getSelectionModel().select(out);
			});
			Button remove = new Button(localization.getString("delete"), StageMain.icon("minus-circle-outline.svg"));
			remove.setMaxWidth(150);
			remove.setOnAction(e -> {
				List<T> removing = new ArrayList<>(list.getSelectionModel().getSelectedItems());
				list.getItems().removeAll(removing);
				if (onDelete != null)
					onDelete.accept(removing);
			});
			buttons.getChildren().addAll(new_, remove);
			return this;
		}

		public ListBuilder<T> addDuplicate(Function<T, T> copier) {
			Button button = new Button(localization.getString("duplicate"), StageMain.icon("content-copy.svg"));
			button.setMaxWidth(150);
			button.setOnAction(e -> list
					.getItems()
					.addAll(list
							.getSelectionModel()
							.getSelectedItems()
							.stream()
							.map(copier)
							.collect(Collectors.toList())));
			buttons.getChildren().addAll(button);
			return this;
		}

		public ListBuilder<T> addButton(Function<ListView<T>, Region> supplier) {
			Region button = supplier.apply(list);
			button.setMaxWidth(150);
			buttons.getChildren().addAll(button);
			return this;
		}

		public ListBuilder<T> moveButtons(Runnable onMove) {
			Button moveUp = new Button(localization.getString("move.up"), StageMain.icon("arrow-up-bold-outline.svg"));
			moveUp.setMaxWidth(150);
			moveUp.setOnAction(e -> {
				Optional<Integer> startIndex =
						list.getSelectionModel().getSelectedIndices().stream().sorted().findFirst();
				if (!startIndex.isPresent())
					return;
				if (startIndex.get() == 0)
					return;
				ArrayList<T> removed = new ArrayList<>(list.getSelectionModel().getSelectedItems());
				list.getItems().removeAll(removed);
				int dest = startIndex.get() - 1;
				list.getItems().addAll(dest, removed);
				list.getSelectionModel().clearSelection();
				list.getSelectionModel().selectRange(dest, dest + removed.size());
				if (onMove != null)
					onMove.run();
			});
			Button moveDown =
					new Button(localization.getString("move.down"), StageMain.icon("arrow-down-bold-outline.svg"));
			moveDown.setMaxWidth(150);
			moveDown.setOnAction(e -> {
				Optional<Integer> startIndex =
						list.getSelectionModel().getSelectedIndices().stream().sorted().findFirst();
				if (!startIndex.isPresent())
					return;
				if (startIndex.get() >= list.getItems().size() - 1)
					return;
				ArrayList<T> removed = new ArrayList<>(list.getSelectionModel().getSelectedItems());
				list.getItems().removeAll(removed);
				int dest = startIndex.get() + 1;
				list.getItems().addAll(dest, removed);
				list.getSelectionModel().clearSelection();
				list.getSelectionModel().selectRange(dest, dest + removed.size());
				if (onMove != null)
					onMove.run();
			});
			buttons.getChildren().addAll(moveUp, moveDown);
			return this;
		}

		public Region build() {
			HBox.setHgrow(list, Priority.ALWAYS);
			HBox topLayout = new HBox();
			topLayout.getChildren().addAll(list, buttons);
			return topLayout;
		}

		public ListBuilder<T> select(T element) {
			list.getSelectionModel().clearSelection();
			list.getSelectionModel().select(element);
			return this;
		}
	}
}
