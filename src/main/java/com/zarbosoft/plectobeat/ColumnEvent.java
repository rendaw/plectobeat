package com.zarbosoft.plectobeat;

public class ColumnEvent {
	public final long start;
	public final long end;
	public final int velocity;
	public HitState startHit = HitState.PENDING;
	public HitState endHit = HitState.PENDING;

	public ColumnEvent(long start, long end, int velocity) {
		this.start = start;
		this.end = end;
		this.velocity = velocity;
	}
}
