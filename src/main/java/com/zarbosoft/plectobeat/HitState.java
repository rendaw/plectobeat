package com.zarbosoft.plectobeat;

public enum HitState {
	PENDING,
	HIT,
	MISS
}
