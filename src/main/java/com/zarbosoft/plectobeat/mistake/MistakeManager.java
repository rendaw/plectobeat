package com.zarbosoft.plectobeat.mistake;

import com.zarbosoft.plectobeat.*;

public abstract class MistakeManager {
	public void reset(Context context) {
	}

	public void processNow(Context context, long now, TimeSignatureEvent timeSignature) {
	}

	public abstract void processEvent(Context context, long now, Column column, ColumnEvent event);

	public boolean detectStartMiss(long now, ColumnEvent event, long detectStartMs) {
		return event.startHit == HitState.PENDING && event.start < now - detectStartMs;
	}

	public boolean detectEndMiss(long now, Column column, ColumnEvent event, long detectEndMs) {
		return column.def.matchNoteEnd && event.endHit == HitState.PENDING && event.end < now - detectEndMs;
	}

	public void stop(Context context) {
	}

	;
}
