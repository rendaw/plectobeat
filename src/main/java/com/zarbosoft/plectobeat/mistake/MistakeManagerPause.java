package com.zarbosoft.plectobeat.mistake;

import com.zarbosoft.plectobeat.Column;
import com.zarbosoft.plectobeat.ColumnEvent;
import com.zarbosoft.plectobeat.Context;
import com.zarbosoft.plectobeat.config.profile.ProfileDef_0_0_1;

public class MistakeManagerPause extends MistakeManager {
	private ProfileDef_0_0_1.MistakeModePause config;

	public MistakeManagerPause(ProfileDef_0_0_1.MistakeModePause config) {
		this.config = config;
	}

	@Override
	public void processEvent(Context context, long now, Column column, ColumnEvent event) {
		if (detectStartMiss(now, event, config.pauseAfter * 1000)) {
			context.startManager.stop(context);
			context.combo.set(0);
		}
		if (detectEndMiss(now, column, event, config.pauseAfter * 1000)) {
			context.startManager.stop(context);
			context.seek(event.start);
		}
	}
}
