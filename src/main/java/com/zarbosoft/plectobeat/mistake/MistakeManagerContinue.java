package com.zarbosoft.plectobeat.mistake;

import com.zarbosoft.plectobeat.Column;
import com.zarbosoft.plectobeat.ColumnEvent;
import com.zarbosoft.plectobeat.Context;
import com.zarbosoft.plectobeat.HitState;

public class MistakeManagerContinue extends MistakeManager {
	@Override
	public void processEvent(Context context, long now, Column column, ColumnEvent event) {
		if (detectStartMiss(now, event, context.profile.detectMs * 1000)) {
			event.startHit = HitState.MISS;
			column.visual.setMiss();
			context.recordMiss();
		}
		if (detectEndMiss(now, column, event, context.profile.detectMs * 1000)) {
			event.endHit = HitState.MISS;
			column.visual.setMiss();
			context.recordMiss();
		}
	}
}
