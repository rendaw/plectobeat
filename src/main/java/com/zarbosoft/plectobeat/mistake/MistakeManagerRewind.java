package com.zarbosoft.plectobeat.mistake;

import com.zarbosoft.plectobeat.*;
import com.zarbosoft.plectobeat.config.profile.ProfileDef_0_0_1;

import java.util.ArrayDeque;
import java.util.Deque;

public class MistakeManagerRewind extends MistakeManager {
	boolean retrying = false;
	boolean retryAbort = false;
	int mistakeCount = 0;
	Deque<BarRecord> barRecords = new ArrayDeque<>();
	Deque<BarRecord> barRecordPool = new ArrayDeque<>();
	TimeSignatureEvent retryTimeSignature = null;
	long retryBar = 0;
	private ProfileDef_0_0_1.MistakeModeRewind config;
	long missAtUsec = Long.MIN_VALUE;

	public MistakeManagerRewind(ProfileDef_0_0_1.MistakeModeRewind config) {
		this.config = config;
	}

	private void endRetry(Context context) {
		retrying = false;
		context.startManager = context.profile.generateStartManager(context);
	}

	@Override
	public void reset(Context context) {
		endRetry(context);
		barRecordPool.addAll(barRecords);
		barRecords.clear();
		mistakeCount = 0;
		missAtUsec = Long.MIN_VALUE;
	}

	@Override
	public void stop(Context context) {
		endRetry(context);
	}

	public void retry(Context context) {
		context.timeSource.stop();

		// Locate where to rewind to
		BarRecord firstMistakeBar = barRecords.stream().filter(barRecord -> barRecord.count > 0).findFirst().get();
		TimeSignatureEvent timeSignature = firstMistakeBar.timeSignature;
		context.seek(timeSignature.localBeatUsec(firstMistakeBar.bar, 0));
		context.startManager.arm(context);

		// Mark where to end retry
		BarRecord lastMistakeBar = barRecords.getLast();
		retryTimeSignature = lastMistakeBar.timeSignature;
		retryBar = lastMistakeBar.bar;

		// Clear records
		barRecordPool.addAll(barRecords);
		barRecords.clear();
		mistakeCount = 0;
	}

	public void recordMiss(Context context, long now, Column column) {
		column.visual.setMiss();
		context.recordMiss();
		if (now == missAtUsec)
			return;
		missAtUsec = now;
		BarRecord lastMistakeBar = barRecords.getLast();
		lastMistakeBar.count += 1;
		mistakeCount += 1;
		if (mistakeCount < config.mistakes)
			return;
		if (config.slow) {
			if (config.cumulativeSlow) {
				if (context.speed.get() - config.slowAmount < 1) {

				} else {
					context.speed.set(context.speed.get() - config.slowAmount);
				}
			} else {
				context.speed.set(context.profile.normalSpeedPercent - config.slowAmount);
			}
		}
		retrying = true;
		retryAbort = true;
		switch (config.startMode) {
			case DELAY:
				context.startManager = config.startModeDelay.generate(false);
				break;
			case TAP:
				context.startManager = config.startModeTap.generate(false);
				break;
		}
		retry(context);
	}

	@Override
	public void processNow(Context context, long now, TimeSignatureEvent timeSignature) {
		retryAbort = false;
		long bar = timeSignature.localUsecBar(now);
		BarRecord lastBarRecord = barRecords.isEmpty() ? null : barRecords.getLast();
		if (config.mistakes == 0)
			return;
		if (lastBarRecord == null || lastBarRecord.timeSignature != timeSignature || lastBarRecord.bar != bar) {
			// If passed retry end, reset speed or return to non-reset mode
			if (lastBarRecord != null &&
					lastBarRecord.timeSignature == retryTimeSignature &&
					lastBarRecord.bar == retryBar) {
				if (retrying) {
					if (config.requirePerfect.get() && mistakeCount > 0) {
						retry(context);
						return;
					} else if (config.slow) {
						if (config.cumulativeRestoreSpeed) {
							if (context.speed.get() + config.slowAmount >= context.profile.normalSpeedPercent) {
								endRetry(context);
								context.speed.set(context.profile.normalSpeedPercent);
								retry(context);
							} else {
								context.speed.add(config.slowAmount);
								retry(context);
							}
						} else {
							endRetry(context);
							context.speed.set(context.profile.normalSpeedPercent);
						}
					} else
						endRetry(context);
				}
			}

			// Add the new record to the stack
			while (barRecords.size() >= config.bars) {
				BarRecord removeBarRecord = barRecords.removeFirst();
				mistakeCount -= removeBarRecord.count;
				barRecordPool.push(removeBarRecord);
			}
			if (barRecordPool.isEmpty())
				barRecordPool.push(new BarRecord());
			BarRecord barRecord;
			barRecords.addLast(barRecord = barRecordPool.pop());
			barRecord.timeSignature = timeSignature;
			barRecord.bar = bar;
			barRecord.count = 0;
		}
	}

	@Override
	public void processEvent(
			Context context, long now, Column column, ColumnEvent event
	) {
		if (config.mistakes == 0 || retryAbort)
			return;
		if (detectStartMiss(now, event, context.profile.detectMs * 1000)) {
			event.startHit = HitState.MISS;
			recordMiss(context, now, column);
		}
		if (detectEndMiss(now, column, event, context.profile.detectMs * 1000)) {
			event.endHit = HitState.MISS;
			recordMiss(context, now, column);
		}
	}

	public static class BarRecord {
		TimeSignatureEvent timeSignature;
		long bar;
		int count;
	}
}
