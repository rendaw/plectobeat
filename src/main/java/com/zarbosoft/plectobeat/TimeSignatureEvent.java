package com.zarbosoft.plectobeat;

public class TimeSignatureEvent {
	public long start;
	public long beatLength;
	public int barMultiple;
	public TimeSignatureEvent previous;
	public int barOffset;

	public long localBeatUsec(long bar, long beat) {
		return start + (bar * barMultiple + beat) * beatLength;
	}

	public long localUsecBeat(long usec) {
		return (usec - start) / beatLength;
	}

	public long localUsecBar(long usec) {
		return localUsecBeat(usec) / barMultiple;
	}

	public long globalUsecBar(long usec) {
		return barOffset + localUsecBar(usec);
	}
}
