package com.zarbosoft.plectobeat.config.trackmeta;

import com.zarbosoft.interface1.Configuration;

import java.nio.file.Path;

@Configuration(name = "0.0.1")
public class TrackMeta_0_0_1 extends TrackMeta {
	@Configuration
	public String name;

	@Configuration(name = "track_shift")
	public long trackShift;

	public String hash;

	public Path audioPath;
	public Path midiPath;

	public long lengthUsec;

	@Override
	public TrackMeta_0_0_1 update() {
		return this;
	}
}
