package com.zarbosoft.plectobeat.config.appstate;

import com.zarbosoft.interface1.Configuration;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.Locale;

@Configuration(name = "0.0.1")
public class AppState_0_0_1 extends AppState {
	@Configuration(name = "last_profile", optional = true)
	public String lastProfile;

	@Configuration
	final public SimpleBooleanProperty audible = new SimpleBooleanProperty(true);

	/**
	 * 0 - 1000
	 */
	@Configuration
	final public SimpleIntegerProperty volume = new SimpleIntegerProperty(1000);

	@Configuration
	public final ObservableList<PlaylistEntry> playlist = FXCollections.observableArrayList();

	@Configuration(name = "last_track", optional = true)
	public SimpleObjectProperty<PlaylistEntry> lastTrack = new SimpleObjectProperty<>();

	/**
	 * In format en-CA
	 */
	@Configuration
	public String locale;

	public static AppState_0_0_1 createDefault() {
		AppState_0_0_1 out = new AppState_0_0_1();
		Locale locale1 = Locale.getDefault();
		out.locale = String.format("%s-%s", locale1.getLanguage(), locale1.getCountry());
		return out;
	}

	@Override
	public AppState_0_0_1 update() {
		return this;
	}

	@Configuration
	public static class PlaylistEntry {
		@Configuration
		public String path;

		@Configuration
		public String name;

		final public SimpleDoubleProperty score = new SimpleDoubleProperty(-1);

		final public SimpleBooleanProperty loaded = new SimpleBooleanProperty(false);
	}
}
