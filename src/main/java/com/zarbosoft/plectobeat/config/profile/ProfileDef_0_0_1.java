package com.zarbosoft.plectobeat.config.profile;

import com.zarbosoft.interface1.Configuration;
import com.zarbosoft.plectobeat.Context;
import com.zarbosoft.plectobeat.USecDuration;
import com.zarbosoft.plectobeat.mistake.*;
import com.zarbosoft.plectobeat.start.StartManager;
import com.zarbosoft.plectobeat.start.StartManagerDelay;
import com.zarbosoft.plectobeat.start.StartManagerTap;
import com.zarbosoft.rendaw.common.DeadCode;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;

import java.util.UUID;

@Configuration(name = "0.0.1")
public class ProfileDef_0_0_1 extends ProfileDef {
	// From profile filename
	public String id;

	@Configuration
	public SimpleStringProperty name = new SimpleStringProperty();

	@Configuration(name = "normal_speed_percent")
	public int normalSpeedPercent;

	@Configuration(name = "detect_ms")
	public int detectMs;

	@Override
	public ProfileDef_0_0_1 update() {
		return this;
	}

	@Configuration
	public static enum MistakeModeSelection {
		@Configuration(name = "ignore") IGNORE,
		@Configuration(name = "continue") CONTINUE,
		@Configuration(name = "pause") PAUSE,
		@Configuration(name = "rewind") REWIND
	}

	@Configuration(name = "mistake_mode")
	public MistakeModeSelection mistakeMode;

	@Configuration(name = "mistake_mode_ignore")
	public MistakeModeIgnore mistakeModeIgnore;

	@Configuration(name = "mistake_mode_continue")
	public MistakeModeContinue mistakeModeContinue;

	@Configuration(name = "mistake_mode_pause")
	public MistakeModePause mistakeModePause;

	@Configuration(name = "mistake_mode_rewind")
	public MistakeModeRewind mistakeModeRewind;

	@Configuration(name = "window_time_before")
	public int windowTimeBefore;

	@Configuration(name = "window_time_after")
	public int windowTimeAfter;

	@Configuration(name = "device_id", optional = true)
	public String deviceId;

	@Configuration
	public static enum StartModeSelection {
		@Configuration(name = "delay") DELAY,
		@Configuration(name = "tap") TAP
	}

	@Configuration(name = "start_mode")
	public StartModeSelection startMode;

	@Configuration(name = "start_mode_delay")
	public StartModeDelay startModeDelay;

	@Configuration(name = "start_mode_tap")
	public StartModeTap startModeTap;

	@Configuration(name = "audio_mode")
	public AudioMode audioMode;

	@Configuration
	public static enum TrackEndMode {
		@Configuration(name = "stop") STOP,
		@Configuration(name = "next_song") NEXT,
		@Configuration(name = "rewind") REWIND,
		@Configuration(name = "rewind_play") REWIND_PLAY,
	}

	@Configuration(name = "on_track_end")
	public TrackEndMode onTrackEnd;

	@Configuration
	public static enum PlaylistEndMode {
		@Configuration(name = "stop") STOP,
		@Configuration(name = "loop_stop") LOOP_STOP,
		@Configuration(name = "loop_play") LOOP_PLAY,
	}

	@Configuration(name = "on_playlist_end")
	public PlaylistEndMode onPlaylistEnd;

	public static ProfileDef_0_0_1 createDefault() {
		ProfileDef_0_0_1 out = new ProfileDef_0_0_1();
		out.id = UUID.randomUUID().toString();
		out.name.set("New profile");
		out.detectMs = 80;
		out.normalSpeedPercent = 100;
		out.windowTimeBefore = 3000;
		out.windowTimeAfter = 1000;
		out.mistakeMode = MistakeModeSelection.REWIND;
		out.mistakeModeContinue = MistakeModeContinue.createDefault();
		out.mistakeModePause = MistakeModePause.createDefault();
		out.mistakeModeRewind = MistakeModeRewind.createDefault();
		out.mistakeModeIgnore = MistakeModeIgnore.createDefault();
		out.startMode = StartModeSelection.TAP;
		out.startModeDelay = StartModeDelay.createDefault();
		out.startModeTap = StartModeTap.createDefault();
		out.audioMode = AudioMode.SONG;
		out.onTrackEnd = TrackEndMode.NEXT;
		out.onPlaylistEnd = PlaylistEndMode.STOP;
		return out;
	}

	public ProfileDef_0_0_1 cloneFull() {
		ProfileDef_0_0_1 out = new ProfileDef_0_0_1();
		out.id = UUID.randomUUID().toString();
		out.name.set(name.get());
		out.detectMs = detectMs;
		out.normalSpeedPercent = normalSpeedPercent;
		out.windowTimeBefore = windowTimeBefore;
		out.windowTimeAfter = windowTimeAfter;
		out.mistakeMode = mistakeMode;
		out.mistakeModeIgnore = mistakeModeIgnore.cloneFull();
		out.mistakeModeContinue = mistakeModeContinue.cloneFull();
		out.mistakeModePause = mistakeModePause.cloneFull();
		out.mistakeModeRewind = mistakeModeRewind.cloneFull();
		out.startMode = startMode;
		out.startModeDelay = startModeDelay.cloneFull();
		out.startModeTap = startModeTap.cloneFull();
		out.audioMode = audioMode;
		out.onTrackEnd = onTrackEnd;
		out.onPlaylistEnd = onPlaylistEnd;
		return out;
	}

	public StartManager generateStartManager(Context context) {
		boolean playing = context.timeSource.playing();
		switch (startMode) {
			case DELAY:
				return startModeDelay.generate(playing);
			case TAP:
				return startModeTap.generate(playing);
			default:
				throw new DeadCode();
		}
	}

	public MistakeManager generateMistakeManager() {
		switch (mistakeMode) {
			case IGNORE:
				return mistakeModeIgnore.generate();
			case CONTINUE:
				return mistakeModeContinue.generate();
			case PAUSE:
				return mistakeModePause.generate();
			case REWIND:
				return mistakeModeRewind.generate();
			default:
				throw new DeadCode();
		}
	}

	@Configuration
	public enum AudioMode {
		@Configuration(name = "song") SONG,
		@Configuration(name = "metronome") METRONOME,
		@Configuration(name = "silent") SILENCE
	}

	@Configuration
	public abstract static class MistakeMode {

		public abstract MistakeManager generate();
	}

	@Configuration(name = "continue")
	public static class MistakeModeContinue extends MistakeMode {

		@Configuration(name = "record_mistakes")
		public boolean recordMistakes;

		public static MistakeModeContinue createDefault() {
			MistakeModeContinue out = new MistakeModeContinue();
			out.recordMistakes = true;
			return out;
		}

		public MistakeModeContinue cloneFull() {
			MistakeModeContinue out = new MistakeModeContinue();
			out.recordMistakes = recordMistakes;
			return out;
		}

		@Override
		public MistakeManager generate() {
			return new MistakeManagerContinue();
		}
	}

	@Configuration(name = "ignore ")
	public static class MistakeModeIgnore extends MistakeMode {

		public static MistakeModeIgnore createDefault() {
			return new MistakeModeIgnore();
		}

		public MistakeModeIgnore cloneFull() {
			return new MistakeModeIgnore();
		}

		@Override
		public MistakeManager generate() {
			return new MistakeManagerIgnore();
		}
	}

	@Configuration(name = "pause")
	public static class MistakeModePause extends MistakeMode {
		@Configuration(name = "retry_note_ends")
		public boolean retryNoteEnds;

		@Configuration(name = "pause_after")
		public long pauseAfter;

		public static MistakeModePause createDefault() {
			MistakeModePause out = new MistakeModePause();
			out.pauseAfter = 20;
			out.retryNoteEnds = false;
			return out;
		}

		public MistakeModePause cloneFull() {
			MistakeModePause out = new MistakeModePause();
			out.pauseAfter = pauseAfter;
			out.retryNoteEnds = retryNoteEnds;
			return out;
		}

		@Override
		public MistakeManager generate() {
			return new MistakeManagerPause(this);
		}

	}

	@Configuration(name = "rewind")
	public static class MistakeModeRewind extends MistakeMode {
		@Configuration
		public int mistakes;

		@Configuration(name = "mistake_beats")
		public int bars;

		@Configuration(name = "retry_note_ends")
		public boolean includeNoteEnds;

		@Configuration(name = "slow")
		public boolean slow;

		@Configuration(name = "slow_amount")
		public int slowAmount;

		@Configuration(name = "cumulative_slow")
		public boolean cumulativeSlow;

		@Configuration(name = "cumulative_restore_speed")
		public boolean cumulativeRestoreSpeed;

		@Configuration(name = "retry_perfect")
		public SimpleBooleanProperty requirePerfect = new SimpleBooleanProperty(false);

		@Configuration(name = "start_mode")
		public StartModeSelection startMode;

		@Configuration(name = "start_mode_delay")
		public StartModeDelay startModeDelay;

		@Configuration(name = "start_mode_tap")
		public StartModeTap startModeTap;

		public static MistakeModeRewind createDefault() {
			MistakeModeRewind out = new MistakeModeRewind();
			out.mistakes = 4;
			out.bars = 2;
			out.includeNoteEnds = false;
			out.slow = false;
			out.slowAmount = 10;
			out.cumulativeSlow = true;
			out.cumulativeRestoreSpeed = true;
			out.startMode = StartModeSelection.DELAY;
			out.startModeDelay = StartModeDelay.createDefault();
			out.startModeTap = StartModeTap.createDefault();
			return out;
		}

		public MistakeModeRewind cloneFull() {
			MistakeModeRewind out = new MistakeModeRewind();
			out.mistakes = mistakes;
			out.bars = bars;
			out.includeNoteEnds = includeNoteEnds;
			out.slow = slow;
			out.slowAmount = slowAmount;
			out.cumulativeSlow = cumulativeSlow;
			out.cumulativeRestoreSpeed = cumulativeRestoreSpeed;
			out.requirePerfect.set(requirePerfect.get());
			out.startMode = startMode;
			out.startModeDelay = startModeDelay.cloneFull();
			out.startModeTap = startModeTap.cloneFull();
			return out;
		}

		@Override
		public MistakeManager generate() {
			return new MistakeManagerRewind(this);
		}

	}

	@Configuration
	public abstract static class StartMode {

		public abstract StartManager generate(boolean playing);
	}

	@Configuration(name = "delay")
	public static class StartModeDelay extends StartMode {
		@Configuration
		public static enum PauseDurationMode {
			@Configuration(name = "bar") BAR,
			@Configuration(name = "clock") CLOCK
		}

		@Configuration
		public PauseDurationMode pauseDuration;

		@Configuration(name = "bar_duration")
		public BarDuration pauseDurationBars;

		@Configuration(name = "clock_duration")
		public ClockDuration pauseDurationClock;

		/**
		 * In ms
		 */
		@Configuration(name = "extra_delay")
		public long extraDelay;

		public static StartModeDelay createDefault() {
			StartModeDelay out = new StartModeDelay();
			out.pauseDuration = PauseDurationMode.BAR;
			out.pauseDurationBars = BarDuration.createDefault();
			out.pauseDurationClock = ClockDuration.createDefault();
			out.extraDelay = 1000;
			return out;
		}

		public StartModeDelay cloneFull() {
			StartModeDelay out = new StartModeDelay();
			out.pauseDuration = pauseDuration;
			out.pauseDurationBars = pauseDurationBars;
			out.pauseDurationClock = pauseDurationClock;
			out.extraDelay = extraDelay;
			return out;
		}

		public TrackDuration getTrackDuration() {
			switch (pauseDuration) {
				case BAR:
					return pauseDurationBars;
				case CLOCK:
					return pauseDurationClock;
				default:
					throw new DeadCode();
			}
		}

		@Override
		public StartManager generate(boolean playing) {
			return new StartManagerDelay(this);
		}

	}

	@Configuration(name = "tap")
	public static class StartModeTap extends StartMode {
		@Configuration
		public int taps;

		/**
		 * Reset tap counter if over this time
		 */
		@Configuration(name = "max_time")
		public long maxTime;

		@Configuration(name = "set_tempo")
		public boolean setTempo;

		/**
		 * 1 = beats
		 * 2 = 2 taps == 1 beat
		 * 5 = 5 taps, maybe 1 slow, 4 fast = time of the fast beat
		 * etc
		 */
		@Configuration(name = "beat_divisions")
		public int beatDivisions;

		public static StartModeTap createDefault() {
			StartModeTap out = new StartModeTap();
			out.taps = 1;
			out.maxTime = 5_000;
			out.setTempo = false;
			out.beatDivisions = 1;
			return out;
		}

		public StartModeTap cloneFull() {
			StartModeTap out = new StartModeTap();
			out.taps = taps;
			out.maxTime = maxTime;
			out.setTempo = setTempo;
			out.beatDivisions = beatDivisions;
			return out;
		}

		@Override
		public StartManager generate(boolean playing) {
			return new StartManagerTap(this, playing);
		}

	}

	@Configuration(name = "bar")
	public static class BarDuration extends TrackDuration {
		@Configuration
		public long bars;

		public static BarDuration createDefault() {
			BarDuration out = new BarDuration();
			out.bars = 1;
			return out;
		}

		@Override
		public USecDuration convert(Context context) {
			return context.relativeBars(-bars);
		}
	}

	@Configuration(name = "clock")
	public static class ClockDuration extends TrackDuration {
		@Configuration
		public long ms;

		public static ClockDuration createDefault() {
			ClockDuration out = new ClockDuration();
			out.ms = 5000;
			return out;
		}

		@Override
		public USecDuration convert(Context context) {
			return new USecDuration(context.lastNow - ms * 1000, ms * 1000);
		}
	}

	@Configuration
	public abstract static class TrackDuration {

		public abstract USecDuration convert(Context context);
	}

}
