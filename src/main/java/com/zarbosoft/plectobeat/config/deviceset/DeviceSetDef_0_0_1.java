package com.zarbosoft.plectobeat.config.deviceset;

import com.google.common.io.BaseEncoding;
import com.zarbosoft.interface1.Configuration;
import com.zarbosoft.plectobeat.ColumnParser;
import com.zarbosoft.plectobeat.Context;
import com.zarbosoft.plectobeat.InputEvent;
import com.zarbosoft.rendaw.common.Common;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.paint.Color;

import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.ShortMessage;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.zarbosoft.plectobeat.Main.localization;
import static com.zarbosoft.plectobeat.Main.logger;
import static com.zarbosoft.rendaw.common.Common.shash1;

@Configuration(name = "0.0.1")
public class DeviceSetDef_0_0_1 extends DeviceSetDef {
	public String id;

	@Configuration
	final public SimpleStringProperty name = new SimpleStringProperty();

	@Configuration
	final public ObservableList<DeviceDef> devices = FXCollections.observableArrayList();

	@Configuration
	final public ObservableList<ColumnDef> columns = FXCollections.observableArrayList();

	public void finish() {
		devices.forEach(d -> d.finish());
	}

	public static DeviceSetDef_0_0_1 createDefault() {
		DeviceSetDef_0_0_1 out = new DeviceSetDef_0_0_1();
		out.id = UUID.randomUUID().toString();
		out.name.set(localization.getString("new.device.set"));
		DeviceDef.midiDevices().forEach(info -> {
			out.devices.add(DeviceDef.createFromInfo(info));
		});
		class Drum {
			final String name;
			final int[] notes;

			Drum(String name, int... notes) {
				this.name = name;
				this.notes = notes.clone();
			}
		}
		Common.Mutable<Integer> lastAngle = new Common.Mutable(0);
		Stream.of(
				new Drum(localization.getString("hh.closed.hit"), 42),
				new Drum(localization.getString("snare"), 38, 40),
				new Drum(localization.getString("bass"), 35, 36),
				new Drum(localization.getString("hh.close"), 44),
				new Drum(localization.getString("hh.open.hit"), 46),
				new Drum(localization.getString("tom.l"), 41, 43, 45),
				new Drum(localization.getString("tom.m"), 47, 48),
				new Drum(localization.getString("tom.h"), 50),
				new Drum(localization.getString("crash"), 49, 57),
				new Drum(localization.getString("splash"), 55),
				new Drum(localization.getString("ride"), 51, 59),
				null
		).filter(d -> d != null).forEach(drum -> {
			ColumnDef columnDef;
			out.columns.add(columnDef = new ColumnDef());
			columnDef.name.set(drum.name);
			columnDef.enabled.set(true);
			columnDef.matchNoteEnd = false;
			Color fillColor = Color.hsb(lastAngle.value, 0.5, 0.5);
			columnDef.fillColor = new RGBAColorDef();
			((RGBAColorDef) columnDef.fillColor).red = (int) (fillColor.getRed() * 255);
			((RGBAColorDef) columnDef.fillColor).green = (int) (fillColor.getGreen() * 255);
			((RGBAColorDef) columnDef.fillColor).blue = (int) (fillColor.getBlue() * 255);
			Color outlineColor = Color.hsb(lastAngle.value, 0.8, 0.8);
			columnDef.outlineColor = new RGBAColorDef();
			((RGBAColorDef) columnDef.outlineColor).red = (int) (outlineColor.getRed() * 255);
			((RGBAColorDef) columnDef.outlineColor).green = (int) (outlineColor.getGreen() * 255);
			((RGBAColorDef) columnDef.outlineColor).blue = (int) (outlineColor.getBlue() * 255);
			((RGBAColorDef) columnDef.outlineColor).alpha = 0;
			lastAngle.value += 25;
			columnDef.detectMs = 0;
			Arrays.stream(drum.notes).forEach(i -> {
				ColumnSource source = new ColumnSource();
				source.track.set(-1);
				source.channel.set(9);
				source.note.set(i);
				columnDef.sources.add(source);
				for (DeviceDef device : out.devices) {
					MidiTrigger trigger = new MidiTrigger();
					trigger.deviceId.set(device.id);
					trigger.channel.set(-1);
					trigger.note.set(i);
					columnDef.triggers.add(trigger);
				}
			});
		});
		out.finish();
		return out;
	}

	public DeviceSetDef_0_0_1 cloneFull() {
		DeviceSetDef_0_0_1 out = new DeviceSetDef_0_0_1();
		out.id = UUID.randomUUID().toString();
		out.name.set(String.format(localization.getString("s.copy"), name.get()));
		out.devices.addAll(devices.stream().map(d -> d.cloneFull()).collect(Collectors.toList()));
		out.columns.addAll(columns.stream().map(c -> c.cloneFull()).collect(Collectors.toList()));
		out.finish();
		return out;
	}

	@Override
	public DeviceSetDef_0_0_1 update() {
		return this;
	}

	@Configuration
	public static class DeviceDef {
		/**
		 * Overwritten on load, serialized to help handwriting configs
		 */
		@Configuration
		public String id;

		@Configuration
		public SimpleStringProperty name = new SimpleStringProperty();

		@Configuration
		public String midiName;

		@Configuration
		public String midiVendor;

		public MidiDevice.Info info;

		public int numericId;

		public static DeviceDef createFromInfo(MidiDevice.Info info) {
			DeviceDef out = new DeviceDef();
			out.name.set(info.getName());
			out.midiName = info.getName();
			out.midiVendor = info.getVendor();
			out.finish();
			return out;
		}

		public DeviceDef cloneFull() {
			DeviceDef out = new DeviceDef();
			out.name.set(name.get());
			out.midiName = midiName;
			out.midiVendor = midiVendor;
			out.finish();
			return out;
		}

		public final void finish() {
			reId();
		}

		public static Map<MidiDevice.Info, Boolean> midiDeviceCache = new HashMap<>();

		public static final Stream<MidiDevice.Info> midiDevices() {
			return Arrays.stream(MidiSystem.getMidiDeviceInfo()).filter(info -> {
				return midiDeviceCache.computeIfAbsent(info, i -> {
					// If we don't know why the device is bad, hope it's good
					MidiDevice midiDevice;
					try {
						midiDevice = MidiSystem.getMidiDevice(info);
					} catch (IllegalArgumentException e) {
						if (e.getMessage().contains("Requested device not installed")) {
							logger.warn(String.format(
									"Unable to open MIDI device [%s / %s / %s]; ignoring",
									info.getName(),
									info.getVendor(),
									info.getVersion()
							), e);
							return false;
						}
						return true;
					} catch (MidiUnavailableException e) {
						return true;
					}
					try {
						midiDevice.open();
					} catch (MidiUnavailableException e) {
						return true;
					}
					try {
						midiDevice.getTransmitter();
					} catch (MidiUnavailableException e) {
						logger.warn(String.format(
								"MIDI device [%s / %s / %s] does not transmit; ignoring",
								info.getName(),
								info.getVendor(),
								info.getVersion()
						), e);
						return false;
					} finally {
						midiDevice.close();
					}
					return true;
				});
			});
		}

		public static int getNumericDeviceId(String deviceId) {
			return ByteBuffer.wrap(BaseEncoding.base64().decode(deviceId)).getInt();
		}

		public static final String hash(DeviceDef def) {
			return shash1(def.midiName, def.midiVendor);
		}

		public static final String hash(MidiDevice.Info def) {
			return shash1(def.getName(), def.getVendor());
		}

		public static final int numericHash(DeviceDef def) {
			return getNumericDeviceId(hash(def));
		}

		public static final int numericHash(MidiDevice.Info def) {
			return getNumericDeviceId(hash(def));
		}

		public final void reId() {
			id = hash(this);
			numericId = numericHash(this);
			info = midiDevices()
					.filter(info -> info.getName().equals(midiName) && info.getVendor().equals(midiVendor))
					.findFirst()
					.orElse(null);
		}

	}

	@Configuration
	public static class ColumnDef {

		@Configuration
		final public SimpleStringProperty name = new SimpleStringProperty();

		@Configuration
		final public SimpleBooleanProperty enabled = new SimpleBooleanProperty();

		@Configuration
		public ColorDef outlineColor;

		@Configuration
		public ColorDef fillColor;

		@Configuration
		final public ObservableList<ColumnSource> sources = FXCollections.observableArrayList();

		@Configuration
		final public ObservableList<ColumnTrigger> triggers = FXCollections.observableArrayList();

		@Configuration
		public int detectMs;

		@Configuration
		public boolean matchNoteEnd;

		public static ColumnDef createDefault() {
			ColumnDef out = new ColumnDef();
			out.name.set("New column");
			out.enabled.set(true);
			out.outlineColor = new RGBAColorDef();
			((RGBAColorDef) out.outlineColor).red = 0;
			((RGBAColorDef) out.outlineColor).green = 0;
			((RGBAColorDef) out.outlineColor).blue = 0;
			out.fillColor = new RGBAColorDef();
			((RGBAColorDef) out.fillColor).red = 128;
			((RGBAColorDef) out.fillColor).green = 128;
			((RGBAColorDef) out.fillColor).blue = 128;
			out.detectMs = 50;
			out.matchNoteEnd = false;
			return out;
		}

		public ColumnDef cloneFull() {
			ColumnDef out = new ColumnDef();
			out.name.set(name.get());
			out.enabled.set(enabled.get());
			out.outlineColor = outlineColor.cloneFull();
			out.fillColor = fillColor.cloneFull();
			out.detectMs = detectMs;
			out.matchNoteEnd = matchNoteEnd;
			out.sources.addAll(sources.stream().map(s -> s.cloneFull()).collect(Collectors.toList()));
			out.triggers.addAll(triggers.stream().map(t -> t.cloneFull()).collect(Collectors.toList()));
			return out;
		}

		public ColumnParser createParser(Context context) {
			return new ColumnParser(context, this);
		}
	}

	@Configuration
	public static class ColumnSource {
		@Configuration
		public SimpleIntegerProperty track = new SimpleIntegerProperty();

		@Configuration
		public SimpleIntegerProperty channel = new SimpleIntegerProperty();

		@Configuration
		public SimpleIntegerProperty note = new SimpleIntegerProperty();

		public ColumnSource cloneFull() {
			ColumnSource out = new ColumnSource();
			out.track.set(track.get());
			out.channel.set(channel.get());
			out.note.set(note.get());
			return out;
		}

		public boolean match(int messageTrack, ShortMessage message) {
			return (track.get() == -1 || track.get() == messageTrack) &&
					(channel.get() == -1 || channel.get() == message.getChannel()) &&
					(note.get() == -1 || note.get() == message.getData1());
		}
	}

	@Configuration
	public abstract static class ColumnTrigger {

		public abstract boolean match(InputEvent inputEvent);

		public abstract ColumnTrigger cloneFull();
	}

	@Configuration(name = "midi")
	public static class MidiTrigger extends ColumnTrigger {
		@Configuration(name = "device_id")
		public SimpleStringProperty deviceId = new SimpleStringProperty();
		public int numericDeviceId;

		@Configuration
		public SimpleIntegerProperty channel = new SimpleIntegerProperty();

		@Configuration
		public SimpleIntegerProperty note = new SimpleIntegerProperty();

		public MidiTrigger() {
			deviceId.addListener((observable, oldValue, newValue) -> {
				if (newValue.isEmpty())
					numericDeviceId = -1;
				else
					numericDeviceId = DeviceDef.getNumericDeviceId(newValue);
			});
		}

		@Override
		public boolean match(InputEvent inputEvent) {
			return (numericDeviceId == -1 || numericDeviceId == inputEvent.device) &&
					(channel.get() == -1 || inputEvent.channel == channel.get()) &&
					(note.get() == -1 || inputEvent.note == note.get());
		}

		public MidiTrigger cloneFull() {
			MidiTrigger out = new MidiTrigger();
			out.deviceId.set(deviceId.getValue());
			out.channel.set(channel.get());
			out.note.set(note.get());
			return out;
		}
	}

	@Configuration(name = "rgb")
	public static class RGBAColorDef extends ColorDef {
		@Configuration
		public int red;

		@Configuration
		public int green;

		@Configuration
		public int blue;

		@Configuration
		public int alpha;

		@Override
		public Color get() {
			return Color.rgb(red, green, blue);
		}

		@Override
		public ColorDef cloneFull() {
			RGBAColorDef out = new RGBAColorDef();
			out.red = red;
			out.green = green;
			out.blue = blue;
			out.alpha = alpha;
			return out;
		}
	}

	@Configuration
	public abstract static class ColorDef {
		public abstract Color get();

		public abstract ColorDef cloneFull();
	}

}
