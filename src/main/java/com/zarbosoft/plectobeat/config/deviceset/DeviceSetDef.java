package com.zarbosoft.plectobeat.config.deviceset;

import com.zarbosoft.interface1.Configuration;

@Configuration
public abstract class DeviceSetDef {
	public abstract DeviceSetDef_0_0_1 update();
}
