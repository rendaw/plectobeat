package com.zarbosoft.plectobeat.timesources;

import com.google.common.io.ByteStreams;
import com.zarbosoft.plectobeat.Context;
import javafx.application.Platform;

import java.nio.file.Path;
import java.time.Instant;
import java.util.Arrays;

import static com.zarbosoft.plectobeat.Main.ffmpegBin;
import static com.zarbosoft.rendaw.common.Common.uncheck;

public class TimeSourceAudio extends TimeSourceAudioBase {
	volatile int bufferPointer = 0;
	volatile double scale = 1;
	volatile boolean resample = false;
	final byte[] song;

	float get16Float(int i) {
		if (bufferPointer + i < 0)
			return 0;
		if (bufferPointer + i * 2 + 1 >= song.length)
			return 0;
		byte l = song[bufferPointer + i * 2];
		byte b = song[bufferPointer + i * 2 + 1];
		return ((int) (b << 8) | (l & 0xFF)) / 32767.0f;
	}

	@Override
	public void fill(int needSamples) {
		int needSourceSamples;
		if (resample) {
			needSourceSamples = (int) (needSamples * scale);
			for (int i = 0; i < needSamples * 2; ++i) {
				double sourceID = i * scale;
				int sourceI = (int) sourceID;
				float percent = (float) (sourceID - sourceI);
				final float before2 = get16Float(sourceI - 2); // 1 back, 2 channels
				final float before1 = get16Float(sourceI);
				final float after1 = get16Float(sourceI + 2);
				final float after2 = get16Float(sourceI + 4); // 2 forward, 2 channels
				final float term1 = after2 - after1 + before1 - before2;
				final float term2 = before2 - before1 - term1;
				final float term3 = after1 - before2;

				short sampled = (short) (
						(
								term1 * percent * percent * percent +
										term2 * percent * percent +
										term3 * percent +
										before1
						) * 30000.0
				);
				resampleBuffer[i * 2] = (byte) (sampled & 0xFF); // LE
				resampleBuffer[i * 2 + 1] = (byte) (sampled >>> 8);
			}
			write(resampleBuffer, 0, needSamples * sampleSize);
		} else {
			needSourceSamples = needSamples;
			if (bufferPointer < 0) {
				int zero = Math.min(-bufferPointer, needSamples * sampleSize);
				Arrays.fill(resampleBuffer, 0, zero, (byte) 0);
				if (zero < needSamples * sampleSize)
					System.arraycopy(song, 0, resampleBuffer, -bufferPointer, (needSamples * sampleSize) - zero);
				write(resampleBuffer, 0, needSamples * sampleSize);
			} else {
				write(song, (int) bufferPointer, needSamples * sampleSize);
			}
		}
		bufferPointer += needSourceSamples * sampleSize;
		//alreadyBuffered += needSamples * sampleSize;
		if (atEnd()) {
			threadAlive = false;
			Platform.runLater(onTrackEnd);
		}
	}

	@Override
	protected void stopped() {
		bufferPointer -= alreadyBuffered;
		alreadyBuffered = 0;
	}

	public TimeSourceAudio(Context context, Path path, Runnable onTrackEnd) {
		super(onTrackEnd);
		if (context.song == null) {
			uncheck(() -> {
				Process ffmpeg = new ProcessBuilder()
						.command(ffmpegBin,
								"-i",
								path.toString(),
								"-f",
								"s16le",
								"-sample_rate",
								"44100",
								"-acodec",
								"pcm_s16le",
								"-ac",
								"2",
								"-"
						)
						.redirectOutput(ProcessBuilder.Redirect.PIPE)
						.redirectInput(ProcessBuilder.Redirect.INHERIT)
						.redirectError(ProcessBuilder.Redirect.INHERIT)
						.start();
				context.song = ByteStreams.toByteArray(ffmpeg.getInputStream());
			});
		}
		song = context.song;
	}

	@Override
	public long getNow() {
		return byteToUsec((int) (bufferPointer - alreadyBuffered * scale)) + (
				playThread == null ? 0 : (long) ((Instant.now().toEpochMilli() - playPointerTime) * scale * 1000)
		);
	}

	@Override
	public void setSpeed(int newValue) {
		scale = newValue / 100.0;
		resample = newValue != 100;
	}

	@Override
	public void seek(long usec) {
		bufferPointer = (int) usecToByte(usec);
		alreadyBuffered = 0;
	}

	@Override
	public boolean atEnd() {
		return bufferPointer - alreadyBuffered >= song.length;
	}
}
