package com.zarbosoft.plectobeat.timesources;

import javax.sound.sampled.*;
import java.time.Instant;
import java.util.Arrays;

import static com.zarbosoft.rendaw.common.Common.uncheck;
import static javax.sound.sampled.FloatControl.Type.MASTER_GAIN;

public abstract class TimeSourceAudioBase extends TimeSource {
	public final SourceDataLine line;
	private final FloatControl volumeControl;
	private final int deviceBufferSize;
	volatile int alreadyBuffered = 0;
	final static int sampleRate = 44100;
	final static int sampleSize = 2 /* 16bit/channel */ * 2 /* channels */;
	public final int limitBufferSize;
	byte[] resampleBuffer;
	public boolean threadAlive = false;
	volatile long playPointerTime = 0;

	byte[] metronome;

	public void write(byte[] source, int offset, int length) {
		line.write(source, offset, length);
		alreadyBuffered += length; // WARNING: Putting this above line.write adds like 10ms of latency to random cycles
		// Maybe it's because volatile is compiler magic
		// Maybe it's because line.write is really slow even when writing less than the empty buffer space
	}

	public abstract void fill(int samples);

	public static long byteToUsec(long offset) {
		return (long) ((offset / sampleSize) * 1_000_000.0 / sampleRate);
	}

	public static long usecToByte(long usec) {
		return (int) (usec * sampleRate / 1_000_000) * sampleSize;
	}

	protected abstract void stopped();

	public class PlayThread extends Thread {
		@Override
		public void run() {
			line.start();
			while (threadAlive) {
				uncheck(() -> Thread.sleep(1));
				int available = line.available();
				alreadyBuffered = deviceBufferSize - available;
				playPointerTime = Instant.now().toEpochMilli();
				int needSamples = Math.min(available, Math.max(limitBufferSize - alreadyBuffered, 0)) / sampleSize;
				if (needSamples < 1000)
					continue;
				fill(needSamples);
			}
			line.stop();
			line.flush();
			stopped();
		}
	}

	PlayThread playThread = null;

	public TimeSourceAudioBase(Runnable onTrackEnd) {
		super(onTrackEnd);
		final AudioFormat format = new AudioFormat((float) 44100, 16, 2, true, false);
		final DataLine.Info lineInfo = new DataLine.Info(SourceDataLine.class, format);
		line = uncheck(() -> AudioSystem.getSourceDataLine(
				format,
				Arrays
						.stream(AudioSystem.getMixerInfo())
						.filter(mixer -> AudioSystem.getMixer(mixer).isLineSupported(lineInfo) &&
								!mixer.getName().toLowerCase().startsWith("java sound"))
						.findFirst()
						.get()
		));
		uncheck(() -> line.open(format));
		volumeControl = (FloatControl) line.getControl(MASTER_GAIN);
		deviceBufferSize = line.getBufferSize();
		limitBufferSize = Math.max((500 * sampleRate / 1000) * sampleSize, lineInfo.getMinBufferSize());
		resampleBuffer = new byte[limitBufferSize];
	}

	@Override
	public long getNow(long animationNow) {
		return getNow();
	}

	@Override
	public boolean stop() {
		if (playThread == null)
			return false;
		threadAlive = false;
		uncheck(() -> playThread.join());
		playThread = null;
		return true;
	}

	@Override
	public void start() {
		if (playThread != null)
			return;
		while (line.available() < deviceBufferSize) {
			uncheck(() -> Thread.sleep(10));
		}
		threadAlive = true;
		alreadyBuffered = 0;
		playThread = new PlayThread();
		playThread.start();
		playPointerTime = Instant.now().toEpochMilli();
	}

	@Override
	public void seek(long usec) {
		alreadyBuffered = 0;
	}

	private static double volConst = (1.0 / Math.E);

	@Override
	public void setVolumeControl(double volumeControl) {
		this.volumeControl.setValue(volumeControl <= 0.0001 ?
				this.volumeControl.getMinimum() :
				(float) (Math.log10(volConst + volumeControl * (1.0 - volConst)) * -this.volumeControl.getMinimum()));
	}

	@Override
	public boolean playing() {
		return threadAlive;
	}
}
