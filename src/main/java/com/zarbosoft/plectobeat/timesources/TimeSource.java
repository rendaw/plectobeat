package com.zarbosoft.plectobeat.timesources;

public abstract class TimeSource {

	final Runnable onTrackEnd;

	public TimeSource(Runnable onTrackEnd) {
		this.onTrackEnd = onTrackEnd;
	}

	/**
	 * @param animationNow
	 * @return the current time within the track in usec
	 */
	public abstract long getNow(long animationNow);

	public abstract long getNow();

	/**
	 * @return true if was playing
	 */
	public abstract boolean stop();

	public abstract void start();

	public abstract void setSpeed(int newValue);

	public abstract boolean atEnd();

	/**
	 * @param usec in usec
	 */
	public abstract void seek(long usec);

	/**
	 * Min 0, max 1
	 *
	 * @param volumeControl
	 */
	public abstract void setVolumeControl(double volumeControl);

	public abstract boolean playing();
}
