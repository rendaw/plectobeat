package com.zarbosoft.plectobeat.timesources;

import javafx.application.Platform;

public class TimeSourceAnimation extends TimeSource {
	private final long length;
	private boolean started;
	private Long startAnimationNow = null;
	private long playStartOffset; // in ms
	private long now = 0;
	private double speed = 1;

	public TimeSourceAnimation(long time, Runnable onTrackEnd) {
		super(onTrackEnd);
		this.length = time;
	}

	@Override
	public long getNow(long animationNow) {
		if (!started)
			return getNow();
		if (startAnimationNow == null) {
			startAnimationNow = animationNow;
		}
		now = playStartOffset + (long) ((animationNow - startAnimationNow) * speed / 1000.0);
		if (now >= length) {
			stop();
			Platform.runLater(onTrackEnd);
		}
		return getNow();
	}

	@Override
	public long getNow() {
		return now;
	}

	@Override
	public boolean stop() {
		boolean out = started;
		startAnimationNow = null;
		playStartOffset = now;
		started = false;
		return out;
	}

	@Override
	public void start() {
		started = true;
	}

	@Override
	public void setSpeed(int newValue) {
		speed = newValue / 100.0;
	}

	@Override
	public void seek(long usec) {
		startAnimationNow = null;
		playStartOffset = now = usec;
	}

	@Override
	public void setVolumeControl(double volumeControl) {

	}

	@Override
	public boolean playing() {
		return started;
	}

	@Override
	public boolean atEnd() {
		return now >= length;
	}
}
