package com.zarbosoft.plectobeat.timesources;

import com.google.common.io.ByteStreams;
import com.zarbosoft.plectobeat.BeatWalker;
import com.zarbosoft.plectobeat.Context;
import com.zarbosoft.plectobeat.Main;
import com.zarbosoft.rendaw.common.Assertion;
import javafx.application.Platform;

import java.time.Instant;

import static com.zarbosoft.rendaw.common.Common.uncheck;

public class TimeSourceMetronome extends TimeSourceAudioBase {
	final byte[] metronome;
	int metronomeSampleOffset;
	final long length;
	volatile long sampleCount;
	double scale = 1;
	BeatWalker beatWalker;

	public TimeSourceMetronome(Context context, long time, Runnable onTrackEnd) {
		super(onTrackEnd);
		beatWalker = new BeatWalker(context);
		length = usecToByte(time) / sampleSize;
		if (context.metronome == null)
			uncheck(() -> {
				context.metronome = ByteStreams.toByteArray(Main.class.getResourceAsStream("metronome.raw"));
			});
		this.metronome = context.metronome;
	}

	public void writeBefore(int amount) {
		int writeMetronome = Math.min(metronome.length / sampleSize - metronomeSampleOffset, amount);
		if (writeMetronome > 0) {
			write(metronome, metronomeSampleOffset * sampleSize, writeMetronome * sampleSize);
			metronomeSampleOffset += writeMetronome;
			amount -= writeMetronome;
		}
		if (amount > 0) {
			write(resampleBuffer, 0, amount * sampleSize);
		}
	}

	@Override
	public void fill(int needSamples) {
		int scaledNeedSamples = (int) (needSamples * scale);
		long startTime = byteToUsec(sampleCount * sampleSize);
		long endTime = byteToUsec(sampleCount * sampleSize + scaledNeedSamples * sampleSize);
		int wrote = 0;
		beatWalker.adjust(startTime, endTime);
		for (BeatWalker.Event beat : beatWalker) {
			int amount = (int) usecToByte((long) ((beat.start - startTime) / scale)) / sampleSize;
			if (amount < 0)
				throw new Assertion();
			writeBefore(amount);
			wrote += amount;
			metronomeSampleOffset = 0;
		}
		writeBefore(needSamples - wrote);
		sampleCount += scaledNeedSamples;
		if (atEnd()) {
			threadAlive = false;
			Platform.runLater(onTrackEnd);
		}
	}

	@Override
	protected void stopped() {
		sampleCount -= alreadyBuffered / sampleSize;
		alreadyBuffered = 0;
	}

	@Override
	public void seek(long usec) {
		super.seek(usec);
		sampleCount = usecToByte(usec) / sampleSize;
	}

	@Override
	public long getNow() {
		return byteToUsec((int) (sampleCount * sampleSize - alreadyBuffered * scale)) + (
				playThread == null ? 0 : (long) ((Instant.now().toEpochMilli() - playPointerTime) * scale * 1000)
		);
	}

	@Override
	public void setSpeed(int newValue) {
		scale = newValue / 100.0;
	}

	@Override
	public boolean atEnd() {
		return sampleCount - alreadyBuffered >= length;
	}
}
