package com.zarbosoft.plectobeat;

import com.zarbosoft.interface1.TypeInfo;
import com.zarbosoft.luxem.write.TypeWriter;
import com.zarbosoft.plectobeat.config.appstate.AppState;
import com.zarbosoft.plectobeat.config.appstate.AppState_0_0_1;
import com.zarbosoft.plectobeat.config.deviceset.DeviceSetDef;
import com.zarbosoft.plectobeat.config.deviceset.DeviceSetDef_0_0_1;
import com.zarbosoft.plectobeat.config.profile.ProfileDef;
import com.zarbosoft.plectobeat.config.profile.ProfileDef_0_0_1;
import com.zarbosoft.plectobeat.config.trackmeta.TrackMeta_0_0_1;
import com.zarbosoft.plectobeat.mistake.MistakeManager;
import com.zarbosoft.plectobeat.start.StartManager;
import com.zarbosoft.plectobeat.timesources.TimeSource;
import com.zarbosoft.plectobeat.timesources.TimeSourceAnimation;
import com.zarbosoft.plectobeat.timesources.TimeSourceAudio;
import com.zarbosoft.plectobeat.timesources.TimeSourceMetronome;
import com.zarbosoft.plectobeat.visual.*;
import com.zarbosoft.rendaw.common.ChainComparator;
import com.zarbosoft.rendaw.common.Pair;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequence;
import javax.sound.midi.Track;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.zarbosoft.rendaw.common.Common.*;

public class Context {
	public AppState_0_0_1 appState;

	public final ObservableList<ProfileDef_0_0_1> profiles = FXCollections.observableArrayList();
	public final ObservableList<DeviceSetDef_0_0_1> devices = FXCollections.observableArrayList();

	public ProfileDef_0_0_1 profile;
	public DeviceSetDef_0_0_1 device;

	public TimeSource timeSource = new TimeSourceAnimation(0, () -> {
	});

	public StartManager startManager;
	public MistakeManager mistakeManager;

	public BlockingQueue<InputEvent> events = new ArrayBlockingQueue(200);
	public BlockingQueue<InputEvent> eventsPool = new ArrayBlockingQueue(200);

	public List<MidiHandle> openDevices = new ArrayList<>();

	public StageProfiles stageProfiles;
	public StageEditProfile stageEditProfile;
	public StageDeviceSets devicesStage;
	public StageEditDeviceSet stageEditDeviceSet;

	public Pane canvas;
	public double height;
	public double width;
	public List<Pair<Label, VisualBar>> barVisuals = IntStream.range(0, 20).mapToObj(i -> {
		Label label = new Label();
		label.setPadding(new Insets(6));
		label.setAlignment(Pos.BOTTOM_LEFT);
		label.setMinHeight(100);
		label.setMinWidth(200);
		label.setTextFill(Color.GRAY);
		label.setVisible(false);
		VisualBar visualBar = new VisualBar(true);
		visualBar.setVisible(false);
		return new Pair<>(label, visualBar);
	}).collect(Collectors.toList());
	public List<VisualBar> beatVisuals = IntStream.range(0, 200).mapToObj(i -> {
		VisualBar visualBar = new VisualBar(false);
		visualBar.setVisible(false);
		return visualBar;
	}).collect(Collectors.toList());

	public TrackMeta_0_0_1 trackMeta;

	public byte[] song;
	public byte[] metronome;

	/**
	 * Guaranteed to have at least 1 element
	 */
	public List<TimeSignatureEvent> timeSignatureEvents = new ArrayList<>();
	public TimeSignatureEvent currentTimeSignature;
	public BeatWalker beatWalker;
	public List<Column> columns = new ArrayList<>();
	public long seekNotes = 0;
	public boolean seek = false;
	public long lastRealNow = 0;
	public long lastNow = 0;
	public SimpleLongProperty timePracticed = new SimpleLongProperty(0);
	public SimpleIntegerProperty combo = new SimpleIntegerProperty(0);
	public SimpleDoubleProperty score = new SimpleDoubleProperty(0.8);
	public SimpleIntegerProperty speed = new SimpleIntegerProperty(100);
	public double floatSpeed = 1;

	Context() {
		speed.addListener((observable, oldValue, newValue) -> {
			floatSpeed = newValue.intValue() / 100.0;
		});
	}

	public void applyVolume() {
		timeSource.setVolumeControl(appState.audible.get() ? (appState.volume.get() / 1000.0) : 0.0);
	}

	public void recordTime(long usec) {
		if (trackMeta == null)
			return;
		if (!timeSource.playing())
			return;
		timePracticed.set(timePracticed.get() + usec);
	}

	public void recordHit() {
		this.combo.set(this.combo.get() + 1);
		this.score.set(Math.min(1.0, score.get() + 0.01));
	}

	public void recordMiss() {
		combo.set(0);
		this.score.set(Math.max(0, score.get() - 0.04));
	}

	public USecDuration relativeBars(long bars) {
		TimeSignatureEvent timeSignature = currentTimeSignature;
		return new USecDuration(
				(
						(int) (lastNow / (timeSignature.beatLength * timeSignature.barMultiple)) + bars
				) * timeSignature.beatLength * timeSignature.barMultiple,
				Math.abs(bars) * timeSignature.barMultiple * timeSignature.beatLength
		);
	}

	public USecDuration relativeBeats(long beats) {
		return relativeBeats(beats, 1);
	}

	public USecDuration relativeBeats(long beats, int divisor) {
		TimeSignatureEvent timeSignature = currentTimeSignature;
		return new USecDuration((
				(int) ((lastNow * divisor) / timeSignature.beatLength) + beats
		) * timeSignature.beatLength / divisor, beats * timeSignature.beatLength / divisor);
	}

	public double msToPixel(long ms) {
		return ms / ((double) (profile.windowTimeBefore + profile.windowTimeAfter) * floatSpeed) * height;
	}

	public double usecToPixel(long usec) {
		return msToPixel(usec / 1000);
	}

	public long pixelToUsec(double pixel) {
		return (long) (pixel * floatSpeed / height * (profile.windowTimeBefore + profile.windowTimeAfter) * 1000);
	}

	public long pixelToMs(double pixel) {
		return pixelToUsec(pixel) / 1000;
	}

	public double columnWidth() {
		if (columns.size() >= 7)
			return width / (columns.size() + 1);
		else
			return width / 7;
	}

	public double eventRadius() {
		double base = Math.min(height, width);
		return Math.min(base / 15 / 2, base / columns.size() / 2 / 2);
	}

	public long loadMidi() {
		return uncheck(() -> {
			if (trackMeta == null)
				return 0L;
			Sequence mySeq = MidiSystem.getSequence(trackMeta.midiPath.toFile());

			List<ColumnParser> columnParsers = new ArrayList<>();
			for (DeviceSetDef_0_0_1.ColumnDef def : device.columns) {
				if (!def.enabled.get())
					continue;
				columnParsers.add(def.createParser(this));
			}

			long lastEventTime = 0;
			for (Pair<Integer, Track> track : iterable(enumerate(Arrays.stream(mySeq.getTracks())))) {
				TimeSignatureParser timeParser = new TimeSignatureParser(this, mySeq);
				for (MidiEvent event : iterable(IntStream
						.range(0, track.second.size())
						.mapToObj(i -> track.second.get(i)))) {
					timeParser.parse(event);

					for (ColumnParser parser : columnParsers)
						parser.parse(track.first, event, timeParser);

				}
				lastEventTime = timeParser.getTime();
			}

			timeSignatureEvents.forEach(e -> e.start += this.trackMeta.trackShift);
			timeSignatureEvents.sort(new ChainComparator<TimeSignatureEvent>().lesserFirst(e -> e.start).build());
			currentTimeSignature = timeSignatureEvents.get(0);
			beatWalker = new BeatWalker(this);

			// Prepare events in each column
			canvas.getChildren().removeAll(columns.stream().map(c -> c.visual).collect(Collectors.toList()));
			columns.clear();
			for (ColumnParser parser : columnParsers) {
				Column column;
				columns.add(column = parser.generate());
				canvas.getChildren().add(column.visual);
			}
			StageMain.resizeAndDistributeColumns(this);

			return lastEventTime;
		});
	}

	private void onTrackEnd() {
		if (appState.lastTrack.get() != null)
			appState.lastTrack.get().score.set(score.getValue());
		switch (profile.onTrackEnd) {
			case STOP:
				startManager.stop(this);
				break;
			case NEXT:
				if (nextTrack())
					startManager.arm(this);
				break;
			case REWIND:
				startManager.stop(this);
				seek(0);
				break;
			case REWIND_PLAY:
				seek(0);
				startManager.arm(this);
				break;
		}
	}

	public void createTimeSource() {
		boolean playing = timeSource.stop();
		long now = 0;
		if (timeSource != null)
			now = timeSource.getNow();
		if (profile.audioMode == ProfileDef_0_0_1.AudioMode.SONG && trackMeta.audioPath != null)
			timeSource = new TimeSourceAudio(this, trackMeta.audioPath, this::onTrackEnd);
		else if (profile.audioMode == ProfileDef_0_0_1.AudioMode.SONG ||
				profile.audioMode == ProfileDef_0_0_1.AudioMode.METRONOME)
			timeSource = new TimeSourceMetronome(this, trackMeta.lengthUsec + 5_000_000, this::onTrackEnd);
		else
			timeSource = new TimeSourceAnimation(trackMeta.lengthUsec + 5_000_000, this::onTrackEnd);
		timeSource.seek(now);
		timeSource.setSpeed(speed.get());
		if (playing)
			timeSource.start();
	}

	public void load(AppState_0_0_1.PlaylistEntry entry) {
		uncheck(() -> {
			reset();
			seek(0);

			Path path = Paths.get(entry.path);
			trackMeta = Main.loadMeta(path);

			trackMeta.lengthUsec = loadMidi();

			song = null;
			createTimeSource();
			lastNow = 0;

			applyVolume();

			if (appState.lastTrack.get() != null)
				appState.lastTrack.get().loaded.set(false);
			entry.loaded.set(true);
			appState.lastTrack.set(entry);
			combo.set(0);
		});
	}

	public TimeSignatureEvent initTimeSignatureEvents() {
		timeSignatureEvents.clear();
		TimeSignatureEvent latestTimeSig;
		timeSignatureEvents.add(latestTimeSig = new TimeSignatureEvent());
		latestTimeSig.start = 0;
		latestTimeSig.beatLength = 60 * 1000 * 1000 / 120;
		latestTimeSig.barMultiple = 4;
		return latestTimeSig;
	}

	public void setProfile(ProfileDef_0_0_1 profile) {
		this.profile = profile;
		appState.lastProfile = profile.id;
		startManager = profile.generateStartManager(this);
		mistakeManager = profile.generateMistakeManager();
		loadMidi();
	}

	public void setDevice(DeviceSetDef_0_0_1 device) {
		closeDevices();
		this.device = device;
		this.profile.deviceId = device.id;
		device.devices.forEach(d -> uncheck(() -> {
			if (d.info == null)
				return;
			MidiHandle handle;
			try {
				handle = new MidiHandle(d.info) {
					@Override
					public void accept(int numericId, int channel, int note, int velocity, boolean on) {
						InputEvent event = eventsPool.poll();
						if (event == null)
							return; // Event spam, give up
						event.device = numericId;
						event.note = note;
						event.velocity = velocity;
						event.on = on;
						events.add(event);
					}
				};
			} catch (MidiHandle.MidiOpenFailed e) {
				Main.logger.warn("Failed to open device for main loop", e);
				return;
			}
			openDevices.add(handle);
		}));
	}

	public void closeDevices() {
		openDevices.forEach(d -> d.close());
		openDevices.clear();
	}

	/**
	 * Set the current time
	 *
	 * @param usec     Move the current time to here
	 * @param noteUsec Start caring about notes from here - events prior to this are marked HIT
	 */
	public void seek(long usec, long noteUsec) {
		timeSource.seek(usec);
		seek = true;
		seekNotes = noteUsec;
		currentTimeSignature = new BeatWalker(this).adjust(noteUsec, noteUsec);
	}

	public void seek(long usec) {
		seek(usec, usec);
	}

	public void previousTrack() {
		startManager.stop(this);
		AppState_0_0_1.PlaylistEntry selected = appState.lastTrack.get();
		do {
			if (selected == null)
				break;
			int index = appState.playlist.indexOf(selected);
			if (index == 0)
				return;
			if (index < 0) {
				selected = null;
				break;
			}
			selected = appState.playlist.get(index - 1);
		} while (false);
		if (selected == null) {
			if (appState.playlist.isEmpty())
				return;
			selected = appState.playlist.get(0);
		}
		load(selected);
	}

	/**
	 * @return if moved to next track (and wasn't handled by the end of playlist)
	 */
	public boolean nextTrack() {
		startManager.stop(this);
		boolean movedWithinPlaylist = true;
		boolean play = false;
		AppState_0_0_1.PlaylistEntry selected = appState.lastTrack.get();
		do {
			if (selected == null)
				break;
			int index = appState.playlist.indexOf(selected);
			if (index >= appState.playlist.size() - 1) {
				movedWithinPlaylist = false;
				switch (profile.onPlaylistEnd) {
					case STOP:
						return false;
					case LOOP_PLAY:
						play = true;
						selected = appState.playlist.get(0);
						break;
					case LOOP_STOP:
						selected = appState.playlist.get(0);
						break;
				}
			} else if (index < 0) {
				selected = null;
				break;
			} else
				selected = appState.playlist.get(index + 1);
		} while (false);
		if (selected == null) {
			if (appState.playlist.isEmpty())
				return false;
			selected = appState.playlist.get(0);
		}
		load(selected);
		if (play)
			startManager.arm(this);
		return movedWithinPlaylist;
	}

	public void transportSkipForward() {
		transportSkipForward(0.8);
	}

	public void transportSkipForward(double scale) {
		reset();
		seek(timeSource.getNow() +
				(long) ((profile.windowTimeBefore + profile.windowTimeAfter) * 1000 * floatSpeed * scale));
	}

	public void transportStop() {
		mistakeManager.stop(this);
		startManager.stop(this);
	}

	public void transportPlay() {
		startManager.start(this);
	}

	public void transportSkipBack() {
		transportSkipBack(0.8);
	}

	public void transportSkipBack(double scale) {
		reset();
		seek(timeSource.getNow() -
				(long) ((profile.windowTimeBefore + profile.windowTimeAfter) * 1000 * floatSpeed * scale));
	}

	public void save() {
		uncheck(() -> {
			try (OutputStream out = Files.newOutputStream(Main.statePath)) {
				new TypeWriter(out, Main.saveTypeMap)
						.pretty((byte) ' ', 4)
						.write(new TypeInfo(AppState.class), appState);
			}
			try {
				Files.list(Main.profilesPath).forEach(p -> uncheck(() -> Files.delete(p)));
				Files.list(Main.devicesPath).forEach(p -> uncheck(() -> Files.delete(p)));
			} finally {
				profiles.forEach(p -> uncheck(() -> {
					try (OutputStream out = Files.newOutputStream(Main.profilesPath.resolve(p.id))) {
						new TypeWriter(out, Main.saveTypeMap)
								.pretty((byte) ' ', 4)
								.write(new TypeInfo(ProfileDef.class), p);
					}
				}));
				devices.forEach(p -> uncheck(() -> {
					try (OutputStream out = Files.newOutputStream(Main.devicesPath.resolve(p.id))) {
						new TypeWriter(out, Main.saveTypeMap)
								.pretty((byte) ' ', 4)
								.write(new TypeInfo(DeviceSetDef.class), p);
					}
				}));
			}
		});
	}

	public void reset() {
		startManager.stop(this);
		mistakeManager.reset(this);
		score.setValue(0.80);
	}
}
