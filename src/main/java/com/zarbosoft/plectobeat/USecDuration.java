package com.zarbosoft.plectobeat;

public class USecDuration {
	public long absolute;
	public long relative;

	public USecDuration(long absolute, long relative) {
		this.absolute = absolute;
		this.relative = relative;
	}
}
