package com.zarbosoft.plectobeat;

import com.zarbosoft.plectobeat.config.deviceset.DeviceSetDef_0_0_1;

import javax.sound.midi.*;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.zarbosoft.rendaw.common.Common.uncheck;
import static javax.sound.midi.ShortMessage.NOTE_OFF;
import static javax.sound.midi.ShortMessage.NOTE_ON;

public abstract class MidiHandle implements Closeable {
	public static class MidiOpenFailed extends RuntimeException {
		public MidiOpenFailed(String message, Throwable e) {
			super(message, e);
		}

		public static MidiOpenFailed format(Throwable e, String format, Object... args) {
			return new MidiOpenFailed(String.format(format, args), e);
		}
	}

	public static Map<MidiDevice.Info, OpenDevice> openDevices = new HashMap<>();
	private OpenDevice device;

	public abstract void accept(int numericId, int channel, int note, int velocity, boolean on);

	private static class OpenDevice {
		private final int numericId;
		List<MidiHandle> handles = new ArrayList<>();
		public final MidiDevice.Info info;
		public final MidiDevice midiDevice;

		private OpenDevice(MidiDevice.Info info) {
			numericId = DeviceSetDef_0_0_1.DeviceDef.numericHash(info);
			this.info = info;
			try {
				midiDevice = uncheck(() -> MidiSystem.getMidiDevice(info));
			} catch (IllegalArgumentException e) {
				if (e.getMessage().contains("Requested device not installed")) {
					throw MidiOpenFailed.format(
							e,
							Main.localization.getString("unable.to.open.midi.device.s.s.s"),
							info.getName(),
							info.getVendor(),
							info.getVersion()
					);
				}
				throw e;
			}
		}

		public void open() {
			try {
				midiDevice.open();
			} catch (MidiUnavailableException e) {
				throw MidiOpenFailed.format(e, Main.localization.getString("midi.device.s.busy"), info);
			}
			Transmitter transmitter;
			try {
				transmitter = midiDevice.getTransmitter();
			} catch (MidiUnavailableException e) {
				midiDevice.close();
				throw MidiOpenFailed.format(
						e,
						Main.localization.getString("unable.to.listen.to.midi.device.s.s.s"),
						info.getName(),
						info.getVendor(),
						info.getVersion()
				);
			}
			transmitter.setReceiver(new Receiver() {
				@Override
				public void send(MidiMessage message1, long timeStamp) {
					if (!(message1 instanceof ShortMessage))
						return;
					ShortMessage message = (ShortMessage) message1;
					if (!(message.getCommand() == NOTE_ON || message.getCommand() == NOTE_OFF))
						return;
					for (MidiHandle handle : handles)
						handle.accept(
								numericId,
								message.getChannel(),
								message.getData1(),
								message.getData2(),
								message.getCommand() == NOTE_ON
						);
				}

				@Override
				public void close() {

				}
			});
		}

		public void close() {
			midiDevice.close();
		}
	}

	public MidiHandle(MidiDevice.Info info) {
		device = openDevices.get(info);
		if (device == null) {
			device = new OpenDevice(info);
			openDevices.put(info, device);
		} else {
			device.close();
		}
		device.handles.add(this);
		device.open();
	}

	@Override
	public void close() {
		device.close();
		device.handles.remove(this);
		if (device.handles.isEmpty()) {
			openDevices.remove(device.info);
		} else {
			device.open();
		}
	}

}
