package com.zarbosoft.plectobeat;

import com.zarbosoft.plectobeat.config.deviceset.DeviceSetDef_0_0_1;
import com.zarbosoft.plectobeat.visual.VisualColumn;
import com.zarbosoft.plectobeat.visual.VisualEvent;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Column {
	public final DeviceSetDef_0_0_1.ColumnDef def;
	public final VisualColumn visual;
	public final List<VisualEvent> eventVisuals =
			IntStream.range(0, 50).mapToObj(i -> new VisualEvent()).collect(Collectors.toList());
	public final List<ColumnEvent> events;
	public int at = 0;

	public Column(DeviceSetDef_0_0_1.ColumnDef def, List<ColumnEvent> events) {
		this.def = def;
		this.events = events;
		visual = new VisualColumn(def);
		visual.getChildren().addAll(0, eventVisuals);
	}

	public void resize(Context context) {
		visual.resize(context);
		eventVisuals.forEach(visualEvent -> visualEvent.setLayoutX(context.columnWidth() / 2 -
				2 * context.eventRadius()));
	}
}
