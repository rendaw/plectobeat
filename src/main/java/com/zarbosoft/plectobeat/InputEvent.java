package com.zarbosoft.plectobeat;

public class InputEvent {
	public int device;
	public int channel;
	public int note;
	public int velocity;
	public boolean on;
}
