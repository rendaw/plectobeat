package com.zarbosoft.plectobeat;

import com.zarbosoft.plectobeat.config.deviceset.DeviceSetDef_0_0_1;
import com.zarbosoft.rendaw.common.ChainComparator;

import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.ShortMessage;
import java.util.ArrayList;
import java.util.List;

import static javax.sound.midi.ShortMessage.NOTE_OFF;
import static javax.sound.midi.ShortMessage.NOTE_ON;

public class ColumnParser {
	private final Context context;
	final DeviceSetDef_0_0_1.ColumnDef def;
	Long start = null;
	int velocity;
	List<ColumnEvent> events = new ArrayList<>();

	public ColumnParser(Context context, DeviceSetDef_0_0_1.ColumnDef def) {
		this.context = context;
		this.def = def;
	}

	void produceEvent(long stop) {
		if (start == null)
			return;
		start += context.trackMeta.trackShift;
		stop += context.trackMeta.trackShift;
		if (start >= 0) {
			events.add(new ColumnEvent(start, stop, velocity));
		}
		start = null;
	}

	public void parse(int track, MidiEvent event, TimeSignatureParser timeSignatureParser) {
		MidiMessage message1 = event.getMessage();
		if (!(message1 instanceof ShortMessage))
			return;
		ShortMessage message = (ShortMessage) message1;
		boolean noteOn = message.getCommand() == NOTE_ON && message.getData2() > 0;
		boolean noteOff =
				message.getCommand() == NOTE_OFF || message.getCommand() == NOTE_ON && message.getData2() == 0;
		if (!noteOn && !noteOff)
			return;
		if (noteOn && start != null)
			return;
		if (!def.sources.stream().anyMatch(s -> s.match(track, message)))
			return;
		if (noteOn) {
			start = timeSignatureParser.getTime();
			velocity = message.getData2();
		}
		if ((noteOn && !def.matchNoteEnd) || (noteOff && def.matchNoteEnd)) {
			produceEvent(timeSignatureParser.getTime());
		}
	}

	public Column generate() {
		if (start != null)
			produceEvent(start);
		events.sort(new ChainComparator<ColumnEvent>().lesserFirst(e -> e.start).build());
		return new Column(def, events);
	}
}
