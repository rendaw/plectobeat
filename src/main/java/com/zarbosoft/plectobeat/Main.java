package com.zarbosoft.plectobeat;

import com.google.common.hash.Hashing;
import com.google.common.io.ByteStreams;
import com.zarbosoft.appdirsj.AppDirs;
import com.zarbosoft.interface1.TypeInfo;
import com.zarbosoft.luxem.Luxem;
import com.zarbosoft.luxem.read.ReadTypeGrammar;
import com.zarbosoft.plectobeat.config.appstate.AppState;
import com.zarbosoft.plectobeat.config.appstate.AppState_0_0_1;
import com.zarbosoft.plectobeat.config.deviceset.DeviceSetDef;
import com.zarbosoft.plectobeat.config.deviceset.DeviceSetDef_0_0_1;
import com.zarbosoft.plectobeat.config.profile.ProfileDef;
import com.zarbosoft.plectobeat.config.profile.ProfileDef_0_0_1;
import com.zarbosoft.plectobeat.config.trackmeta.TrackMeta;
import com.zarbosoft.plectobeat.config.trackmeta.TrackMeta_0_0_1;
import com.zarbosoft.plectobeat.visual.StageMain;
import io.github.classgraph.ClassGraph;
import io.github.classgraph.ScanResult;
import javafx.application.Application;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sound.midi.*;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.IntStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static com.zarbosoft.rendaw.common.Common.uncheck;

public class Main extends Application {
	public static Logger logger = LoggerFactory.getLogger("root");
	public static String appId = "plectobeat";
	public static String songExtension = "plectotrack";
	public static String appName = "Plectobeat";
	public static AppDirs appDirs = new AppDirs().set_appname(appId).set_appauthor("zarbosoft");
	public static Path profilesPath = appDirs.user_config_dir().resolve("profiles");
	public static Path devicesPath = appDirs.user_config_dir().resolve("devices");
	public static Path statePath = appDirs.user_config_dir().resolve("state");
	public static Path trackPath = appDirs.user_config_dir().resolve("tracks");
	public static ScanResult reflections = new ClassGraph().enableAllInfo().whitelistPackages("com.zarbosoft").scan();

	public static ResourceBundle localization;

	public static Timer timer = new Timer();
	public static String ffmpegBin;

	public static ReadTypeGrammar.TypeMap saveTypeMap = new ReadTypeGrammar.TypeMap()
			.add(new TypeInfo(SimpleIntegerProperty.class), new ReadTypeGrammar.TypeMapEntry<SimpleIntegerProperty>() {
				@Override
				public void setIn(Object object, TypeInfo field, Object value) {
					uncheck(() -> ((SimpleIntegerProperty) field.field.get(object)).set((Integer) value));
				}

				@Override
				public Object convertOut(SimpleIntegerProperty source) {
					return source.get();
				}

				@Override
				public TypeInfo serializedType(TypeInfo info) {
					return new TypeInfo(info.field, int.class);
				}
			})
			.add(new TypeInfo(SimpleStringProperty.class), new ReadTypeGrammar.TypeMapEntry<SimpleStringProperty>() {
				@Override
				public void setIn(Object object, TypeInfo field, Object value) {
					uncheck(() -> ((SimpleStringProperty) field.field.get(object)).set((String) value));
				}

				@Override
				public Object convertOut(SimpleStringProperty source) {
					return source.get();
				}

				@Override
				public TypeInfo serializedType(TypeInfo info) {
					return new TypeInfo(info.field, String.class);
				}
			})
			.add(new TypeInfo(SimpleBooleanProperty.class), new ReadTypeGrammar.TypeMapEntry<SimpleBooleanProperty>() {
				@Override
				public void setIn(Object object, TypeInfo field, Object value) {
					uncheck(() -> ((SimpleBooleanProperty) field.field.get(object)).set((Boolean) value));
				}

				@Override
				public Object convertOut(SimpleBooleanProperty source) {
					return source.get();
				}

				@Override
				public TypeInfo serializedType(TypeInfo info) {
					return new TypeInfo(info.field, Boolean.class);
				}
			})
			.add(new TypeInfo(SimpleObjectProperty.class), new ReadTypeGrammar.TypeMapEntry<SimpleObjectProperty>() {
				@Override
				public void setIn(Object object, TypeInfo field, Object value) {
					uncheck(() -> ((SimpleObjectProperty) field.field.get(object)).set(value));
				}

				@Override
				public Object convertOut(SimpleObjectProperty source) {
					return source.get();
				}

				@Override
				public TypeInfo serializedType(TypeInfo info) {
					return new TypeInfo(info.field, info.parameters[0].type, info.parameters[0].parameters);
				}
			})
			.add(new TypeInfo(ObservableList.class), new ReadTypeGrammar.TypeMapEntry<ObservableList>() {
				@Override
				public void setIn(Object object, TypeInfo field, Object value) {
					uncheck(() -> ((ObservableList) field.field.get(object)).setAll((Collection) value));
				}

				@Override
				public TypeInfo serializedType(TypeInfo info) {
					return new TypeInfo(info.field, ArrayList.class, info.parameters);
				}
			});

	public static void main(String[] args) {
		launch(args);
	}

	public static TrackMeta_0_0_1 loadMeta(Path path) {
		return uncheck(() -> {
			final String hash = com.google.common.io.Files.asByteSource(path.toFile()).hash(Hashing.md5()).toString();
			final Path audioPath;
			final Path midiPath;
			Optional<TrackMeta_0_0_1> trackMeta = Optional.empty();
			if (path.getFileName().toString().endsWith("." + songExtension)) {
				Path tempRoot = Paths.get(System.getProperty("java.io.tmpdir")).resolve(appId);
				Path songsRoot = tempRoot.resolve("songs");
				Path songRoot = songsRoot.resolve(hash);

				if (Files.exists(songRoot)) {

				} else {
					Files.createDirectories(songRoot);
					try (ZipFile zip = new ZipFile(path.toFile())) {
						Enumeration<? extends ZipEntry> entries = zip.entries();
						for (; entries.hasMoreElements(); ) {
							ZipEntry e = entries.nextElement();
							if (e.isDirectory())
								continue;
							try (OutputStream extract = Files.newOutputStream(songRoot.resolve(e.getName()))) {
								ByteStreams.copy(zip.getInputStream(e), extract);
							}
						}
					}
				}

				Path metaPath = songRoot.resolve("meta.luxem");
				if (Files.exists(metaPath))
					try (InputStream metaStream = Files.newInputStream(metaPath)) {
						trackMeta = Luxem.<TrackMeta>parse(reflections, new TypeInfo(TrackMeta.class))
								.map(saveTypeMap)
								.from(metaStream)
								.findFirst()
								.map(m -> m.update());
					}

				audioPath = Files
						.list(songRoot)
						.filter(p -> p.getFileName().toString().startsWith("audio."))
						.findFirst()
						.orElse(null);
				midiPath = songRoot.resolve("midi.mid");
			} else {
				midiPath = path;
				audioPath = null;
			}

			// Prep meta
			TrackMeta_0_0_1 out = trackMeta.orElseGet(() -> {
				TrackMeta_0_0_1 m = new TrackMeta_0_0_1();
				Sequence seq = uncheck(() -> MidiSystem.getSequence(midiPath.toFile()));
				for (Track track : seq.getTracks()) {
					for (int i = 0; i < track.size(); ++i) {
						MidiEvent event = track.get(i);
						if (event.getTick() > 0)
							break;
						MidiMessage message = event.getMessage();
						if (!(message instanceof MetaMessage))
							break;
						if (((MetaMessage) message).getType() != 0x03)
							break;
						m.name = new String(((MetaMessage) message).getData(), StandardCharsets.UTF_8);
						break;
					}
					if (m.name != null)
						break;
				}
				if (m.name == null)
					m.name = path.getFileName().toString();
				m.trackShift = 0;
				return m;
			});
			out.midiPath = midiPath;
			out.audioPath = audioPath;
			out.hash = hash;
			return out;
		});
	}

	Context context = new Context();

	public void start(Stage primaryStage) throws Exception {
		// Load app state
		try {
			try (InputStream stateFile = Files.newInputStream(statePath)) {
				context.appState = Luxem.<AppState>parse(reflections, new TypeInfo(AppState.class))
						.map(saveTypeMap)
						.from(stateFile)
						.findFirst()
						.map(state -> state.update())
						.get();
			}
		} catch (Exception e) {
			logger.error(String.format("Failed to open state file [%s]", statePath), e);
			context.appState = AppState_0_0_1.createDefault();
		}

		// Load localization
		class UTF8Control extends ResourceBundle.Control {
			// From https://stackoverflow.com/questions/4659929/how-to-use-utf-8-in-resource-properties-with-resourcebundle
			public ResourceBundle newBundle(
					String baseName, Locale locale, String format, ClassLoader loader, boolean reload
			) throws IllegalAccessException, InstantiationException, IOException {
				// The below is a copy of the default implementation.
				String bundleName = toBundleName(baseName, locale);
				String resourceName = toResourceName(bundleName, "properties");
				ResourceBundle bundle = null;
				InputStream stream = null;
				if (reload) {
					URL url = loader.getResource(resourceName);
					if (url != null) {
						URLConnection connection = url.openConnection();
						if (connection != null) {
							connection.setUseCaches(false);
							stream = connection.getInputStream();
						}
					}
				} else {
					stream = loader.getResourceAsStream(resourceName);
				}
				if (stream != null) {
					try {
						// Only this line is changed to make it to read properties files as UTF-8.
						bundle = new PropertyResourceBundle(new InputStreamReader(stream, "UTF-8"));
					} finally {
						stream.close();
					}
				}
				return bundle;
			}
		}
		String[] localeParts = context.appState.locale.split("-");
		Locale locale = new Locale(localeParts[0], localeParts[1]);
		try {
			localization =
					ResourceBundle.getBundle("com.zarbosoft.plectobeat.i18n.messages", locale, new UTF8Control());
		} catch (MissingResourceException e) {
			localization = ResourceBundle.getBundle("com.zarbosoft.plectobeat.i18n.messages",
					new Locale("en", "US"),
					new UTF8Control()
			);
		}

		// Prep bundled music
		{
			Files.createDirectories(trackPath);
			List<String> tracks = new ClassGraph()
					.enableAllInfo()
					.whitelistPackages("com.zarbosoft.plectobeat.tracks")
					.scan()
					.getAllResources()
					.getPaths();
			for (String trackString : tracks) {
				Path sourcePath = Paths.get(trackString);
				Path destPath = trackPath.resolve(sourcePath.getFileName());
				if (!Files.exists(destPath)) {
					try (OutputStream out = Files.newOutputStream(destPath)) {
						ByteStreams.copy(Main.class.getClassLoader().getResourceAsStream(sourcePath.toString()), out);
					}
				}
			}
		}

		// Prep ffmpeg
		if (appDirs.get_system() == AppDirs.SystemEnum.LINUX2)
			ffmpegBin = "ffmpeg";
		else {
			String name = appDirs.get_system() == AppDirs.SystemEnum.WIN32 ? "ffmpeg.exe" : "ffmpeg";
			Path cacheDir = appDirs.user_cache_dir(true);
			Files.createDirectories(cacheDir);
			Path bin = cacheDir.resolve(name);
			if (!Files.exists(bin))
				try (OutputStream binOut = Files.newOutputStream(bin)) {
					ByteStreams.copy(Main.class.getResourceAsStream(name), binOut);
				}
			ffmpegBin = bin.toString();
		}

		// Load profiles
		Files.createDirectories(profilesPath);
		Files.list(profilesPath).forEach(profilePath -> uncheck(() -> {
			try (InputStream profileFile = Files.newInputStream(profilePath)) {
				ProfileDef def1 = Luxem.<ProfileDef>parse(reflections, new TypeInfo(ProfileDef.class))
						.map(saveTypeMap)
						.from(profileFile)
						.findFirst()
						.get();
				ProfileDef_0_0_1 def = def1.update();
				def.id = profilePath.getFileName().toString();
				context.profiles.add(def);
			} catch (Exception e) {
				logger.warn(String.format(localization.getString("failed.to.load.profile.s"), profilePath), e);
			}
		}));

		// Load devices
		Files.createDirectories(devicesPath);
		Files.list(devicesPath).forEach(devicePath -> uncheck(() -> {
			try (InputStream deviceFile = Files.newInputStream(devicePath)) {
				DeviceSetDef def1 = Luxem.<DeviceSetDef>parse(reflections, new TypeInfo(DeviceSetDef.class))
						.map(saveTypeMap)
						.from(deviceFile)
						.findFirst()
						.get();
				DeviceSetDef_0_0_1 def = def1.update();
				def.id = devicePath.getFileName().toString();
				context.devices.add(def);
				def.finish();
			} catch (Exception e) {
				logger.warn(String.format(localization.getString("failed.to.load.device.s"), devicePath), e);
			}
		}));

		// Select profile
		{
			ProfileDef_0_0_1 useProfile = context.profiles
					.stream()
					.filter(profileDef -> Objects.equals(profileDef.id, context.appState.lastProfile))
					.findFirst()
					.orElse(null);
			if (useProfile == null) {
				if (!context.profiles.isEmpty())
					useProfile = context.profiles.get(0);
				else {
					useProfile = ProfileDef_0_0_1.createDefault();
					context.profiles.add(useProfile);
				}
			}
			context.setProfile(useProfile);
		}

		// Select device
		{
			DeviceSetDef_0_0_1 useDevice = context.devices
					.stream()
					.filter(deviceDef -> Objects.equals(deviceDef.id, context.profile.deviceId))
					.findFirst()
					.orElse(null);
			if (useDevice == null) {
				if (!context.devices.isEmpty()) {
					useDevice = context.devices.get(0);
				} else {
					context.devices.add(useDevice = DeviceSetDef_0_0_1.createDefault());
				}
			}
			context.setDevice(useDevice);
		}

		//
		context.initTimeSignatureEvents();
		context.beatWalker = new BeatWalker(context);

		IntStream.range(0, 200).forEach(i -> context.eventsPool.add(new InputEvent()));

		StageMain.setup(context, primaryStage);
	}

	public static Map<String, Image> iconCache = new HashMap<>();

	@Override
	public void stop() throws Exception {
		timer.cancel();
		context.timeSource.stop();
		context.closeDevices();
		context.save();
	}
}
