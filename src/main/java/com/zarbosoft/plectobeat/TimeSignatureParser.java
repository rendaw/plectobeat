package com.zarbosoft.plectobeat;

import javax.sound.midi.MetaMessage;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.Sequence;

public class TimeSignatureParser {
	final Context context;
	final Sequence mySeq;
	long tick;

	int quarterNoteUSec = 1;
	int beatsPerBar = 4;
	int barBeatDivisor = 4;
	TimeSignatureEvent latestTimeSig;
	long latestTimeSigAtTick = 0;

	TimeSignatureParser(Context context, Sequence mySeq) {
		this.context = context;
		this.mySeq = mySeq;
		latestTimeSig = context.initTimeSignatureEvents();
	}

	long getTime() {
		if (mySeq.getDivisionType() == Sequence.PPQ)
			return latestTimeSig.start +
					(tick - latestTimeSigAtTick) * latestTimeSig.beatLength / mySeq.getResolution();
		else
			return (long) (tick * 1000_000.0 / (mySeq.getDivisionType() * mySeq.getResolution()));
	}

	void parse(MidiEvent event) {
		tick = event.getTick();
		long time = getTime();
		do {
			MidiMessage message1 = event.getMessage();
			if (!(message1 instanceof MetaMessage))
				break;
			MetaMessage message = (MetaMessage) message1;
			byte[] data = message.getData();
			if (message.getType() == 0x58) {
				beatsPerBar = data[0];
				barBeatDivisor = 1 << data[1];
			} else if (message.getType() == 0x51) {
				quarterNoteUSec = (data[2] & 0xFF) | ((data[1] & 0xFF) << 8) | ((data[0] & 0x0F) << 16);
			} else
				break;
			if (latestTimeSigAtTick != event.getTick()) {
				TimeSignatureEvent previous = latestTimeSig;
				context.timeSignatureEvents.add(latestTimeSig = new TimeSignatureEvent());
				latestTimeSig.start = time;
				latestTimeSigAtTick = event.getTick();
				latestTimeSig.previous = previous;
				latestTimeSig.barOffset =
						previous == null ? 0 : (previous.barOffset + (int) previous.localUsecBar(time - 1) + 1);
			}
			latestTimeSig.beatLength = 4 * quarterNoteUSec / barBeatDivisor;
			latestTimeSig.barMultiple = beatsPerBar;
		} while (false);
	}
}
