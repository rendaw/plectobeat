package com.zarbosoft.plectobeat.start;

import com.zarbosoft.plectobeat.Column;
import com.zarbosoft.plectobeat.Context;
import com.zarbosoft.plectobeat.InputEvent;
import com.zarbosoft.plectobeat.USecDuration;
import com.zarbosoft.plectobeat.config.deviceset.DeviceSetDef_0_0_1;
import com.zarbosoft.plectobeat.config.profile.ProfileDef_0_0_1;
import com.zarbosoft.rendaw.common.Assertion;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StartManagerTap extends StartManager {
	boolean playing;
	int tapCount = 0;
	List<Long> tapTimes = new ArrayList<>();
	private ProfileDef_0_0_1.StartModeTap config;

	public StartManagerTap(ProfileDef_0_0_1.StartModeTap config, boolean playing) {
		this.config = config;
		this.playing = playing;
	}

	@Override
	public void stop(Context context) {
		context.timeSource.stop();
		playing = false;
	}

	@Override
	public void arm(Context context) {
		context.timeSource.stop();
		playing = false;
	}

	@Override
	public void start(Context context) {
		playing = true;
		context.timeSource.start();
	}

	@Override
	public void update(Context context) {
		if (playing)
			return;
		boolean matched = false;
		for (InputEvent event : context.events) {
			for (Column column : context.columns) {
				for (DeviceSetDef_0_0_1.ColumnTrigger trigger : column.def.triggers) {
					if (!trigger.match(event))
						continue;
					matched = true;
					break;
				}
				if (matched)
					break;
			}
		}
		if (!matched)
			return;
		long now = Instant.now().toEpochMilli();
		if (tapCount > 0 && (now - tapTimes.get(tapCount - 1)) < context.profile.detectMs)
			return;
		while (tapTimes.size() <= tapCount)
			tapTimes.add(0L);
		tapTimes.set(tapCount++, now);
		if (tapCount >= 2 && now - tapTimes.get(0) > config.maxTime * 1000)
			tapCount = 0;
		else if (tapCount == config.taps) {
			if (config.setTempo) {
				Long lastAbs = null;
				long diffs[] = new long[tapTimes.size() - 1];
				int atDiff = -1;
				// Find time deltas between taps
				for (int tapIndex = 0; tapIndex < config.taps; ++tapIndex) {
					long tap = tapTimes.get(tapIndex);
					if (lastAbs != null) {
						long diff = tap - lastAbs;

						// Compute the multiple of the previous delta to the current
						// Adjust all times to the smallest beat length
						if (atDiff >= 0) {
							if (diffs[atDiff] < diff) {
								long multiple = Math.max(1, Math.round(diff / (double) diffs[atDiff]));
								diff /= multiple;
							} else {
								long multiple = Math.max(1, Math.round(diffs[atDiff] / (double) diff));
								for (int i = 0; i <= atDiff; ++i)
									diffs[i] /= multiple;
							}
						}
						diffs[++atDiff] = diff;
					}
					lastAbs = tap;
				}

				// Treat (median) smallest beat time as 1 bar subdivision, adjust time as such
				Arrays.sort(diffs);
				double median;
				if (diffs.length % 2 == 0)
					median = (diffs[diffs.length / 2] + diffs[diffs.length / 2 - 1]) / 2.0;
				else
					median = diffs[diffs.length / 2];
				int newSpeed =
						(int) (100 * context.currentTimeSignature.beatLength / (median * 1000 * config.beatDivisions));
				if (newSpeed <= 0 || newSpeed > 1000)
					throw new Assertion();
				context.timeSource.setSpeed(newSpeed);
			}
			playing = true;
			tapCount = 0;
			context.timeSource.stop();
			USecDuration duration = context.relativeBeats(-1);
			context.seek(duration.absolute, duration.absolute + duration.relative);
			context.timeSource.start();
		}
	}
}
