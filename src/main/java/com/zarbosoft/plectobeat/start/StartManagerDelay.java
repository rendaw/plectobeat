package com.zarbosoft.plectobeat.start;

import com.zarbosoft.plectobeat.Context;
import com.zarbosoft.plectobeat.Main;
import com.zarbosoft.plectobeat.USecDuration;
import com.zarbosoft.plectobeat.config.profile.ProfileDef_0_0_1;
import javafx.application.Platform;

import java.util.TimerTask;

public class StartManagerDelay extends StartManager {
	TimerTask task;
	private ProfileDef_0_0_1.StartModeDelay config;

	public StartManagerDelay(ProfileDef_0_0_1.StartModeDelay config) {
		this.config = config;
	}

	@Override
	public void stop(Context context) {
		if (task != null)
			task.cancel();
		if (context.timeSource.stop()) {
		}
	}

	@Override
	public void arm(Context context) {
		context.timeSource.stop();
		USecDuration duration = config.getTrackDuration().convert(context);
		context.seek(duration.absolute, duration.absolute + duration.relative);
		Main.timer.schedule(task = new TimerTask() {
			@Override
			public void run() {
				task = null;
				Platform.runLater(() -> {
					context.timeSource.start();
				});
			}
		}, config.extraDelay);
	}

	@Override
	public void start(Context context) {
		arm(context);
	}

	@Override
	public void update(Context context) {
	}
}
