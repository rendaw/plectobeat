package com.zarbosoft.plectobeat.start;

import com.zarbosoft.plectobeat.Context;

public abstract class StartManager {
	public abstract void stop(Context context);

	public abstract void arm(Context context);

	public abstract void start(Context context);

	public abstract void update(Context context);
}
