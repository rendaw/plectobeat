package com.zarbosoft.plectobeat;

import java.util.List;
import java.util.NoSuchElementException;

/**
 * Iterate beats within a time region.  Caches position for fast subsequent nearby iterations.
 */
public class BeatWalker implements Iterable<BeatWalker.Event> {
	private final Iterator iterator;

	public BeatWalker(Context context) {
		this.iterator = new Iterator(context.timeSignatureEvents);
	}

	public static class Event {
		public TimeSignatureEvent timeSignature;
		public long start;
		public long index;
	}

	public static class Iterator implements java.util.Iterator<Event> {
		private final Event event;
		private final List<TimeSignatureEvent> events;

		// Always
		int at = 0;

		// After reset
		long windowStart;
		long windowEnd;
		TimeSignatureEvent timeSignature;
		int timeSignatureAt;
		long nextTimeSignatureTime;
		long beatIndex;
		long beatTime;

		public Iterator(List<TimeSignatureEvent> timeSignatureEvents) {
			this.events = timeSignatureEvents;
			this.event = new Event();
		}

		public TimeSignatureEvent reset(long windowStart, long windowEnd) {
			this.windowStart = windowStart;
			this.windowEnd = windowEnd;

			// Move time signatures until the first considered time signature starts before the window
			timeSignature = events.get(at);
			while (timeSignature.start < windowStart) {
				at += 1;
				if (at >= events.size()) {
					at -= 1;
					break;
				} else {
					timeSignature = events.get(at);
				}
			}
			while (timeSignature.start > windowStart) {
				if (at == 0)
					break;
				at -= 1;
				timeSignature = events.get(at);
			}

			TimeSignatureEvent out = timeSignature;

			timeSignatureAt = at;
			nextTimeSignatureTime =
					timeSignatureAt + 1 < events.size() ? events.get(timeSignatureAt + 1).start : Long.MAX_VALUE;
			beatIndex = Math.floorDiv(windowStart - timeSignature.start - 1, timeSignature.beatLength) + 1;
			update();
			return out;
		}

		public void update() {
			// Move to the next time signature if the beat is past it's start
			beatTime = timeSignature.localBeatUsec(0, beatIndex);
			if (beatTime >= nextTimeSignatureTime) {
				timeSignatureAt += 1;
				timeSignature = events.get(timeSignatureAt);
				nextTimeSignatureTime =
						timeSignatureAt + 1 < events.size() ? events.get(timeSignatureAt + 1).start : Long.MAX_VALUE;
				beatIndex = 0;
				beatTime = timeSignature.start;
			}
		}

		@Override
		public boolean hasNext() {
			return beatTime < windowEnd;
		}

		@Override
		public Event next() {
			if (!hasNext())
				throw new NoSuchElementException();
			event.timeSignature = timeSignature;
			event.index = beatIndex;
			event.start = beatTime;
			beatIndex += 1;
			update();
			return event;
		}
	}

	@Override
	public java.util.Iterator<Event> iterator() {
		return iterator;
	}

	public TimeSignatureEvent adjust(long start, long stop) {
		return iterator.reset(start, stop);
	}
}
